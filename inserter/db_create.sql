/* provided for consistency
CREATE DATABASE tvbarometer WITH ENCODING='UTF8' CONNECTION LIMIT=-1;
USE tvbarometer;
*/

/* if you modify the table attribute types, don't forget to update the procedure parameters as well */

/* normalized users storage */
CREATE TABLE Users (
	userId char(36) NOT NULL PRIMARY KEY,
	accountId varchar(10) NOT NULL,
	refNumber varchar(10) NOT NULL,
	firstName varchar(20) NOT NULL,
	lastName varchar(30) NOT NULL,
	username varchar(25) UNIQUE NOT NULL
);

/* list of emission times - used to construct a complete x-axis on a chart */
CREATE TABLE Emissions (
	channelName varchar(35) NOT NULL,
	programmeStart timestamp NOT NULL,
	programmeEnd timestamp NOT NULL,
	programmeName varchar(120) NOT NULL,
	PRIMARY KEY (channelName, programmeStart)
);

/* total time watched for programmes per user */
/* could be normalized by referencing Emissions by (chName, prgStart) instead of additionally storing prgEnd and prgName, however since Emissions is filled separately the resulting dependency is currently undesired */
CREATE TABLE ProgrammesUsersTTW (
	userId char(36) NOT NULL,
	channelName varchar(35) NOT NULL,
	programmeStart timestamp NOT NULL,
	programmeEnd timestamp NOT NULL,
	programmeName varchar(120) NOT NULL,
	timeWatched real NOT NULL,	/* in minutes */
	PRIMARY KEY (userId, channelName, programmeStart)
);

/* insert function for Emissions */
CREATE OR REPLACE FUNCTION InsertOrUpdateEmission(chName varchar(35), prgStart timestamp, prgEnd timestamp, prgName varchar(120)) RETURNS integer AS $$
DECLARE 
	ret integer;
BEGIN
	IF chName IS NOT NULL AND prgStart IS NOT NULL AND prgEnd IS NOT NULL AND prgName IS NOT NULL THEN
		IF EXISTS (SELECT * FROM Emissions WHERE Emissions.channelName = chName AND Emissions.programmeStart = prgStart) THEN
			UPDATE Emissions SET programmeEnd = prgEnd, programmeName = prgName WHERE Emissions.channelName = chName AND Emissions.programmeStart = prgStart;
		ELSE
			INSERT INTO Emissions (channelName, programmeStart, programmeEnd, programmeName) VALUES (chName, prgStart, prgEnd, prgName);
		END IF;
		
		GET DIAGNOSTICS ret = ROW_COUNT;
		RETURN ret;
	ELSE 
		RETURN NULL;
	END IF;
END;
$$ LANGUAGE plpgsql;

/* insert function for ProgrammesUsersTTW */
CREATE OR REPLACE FUNCTION InsertOrUpdateUserTTW(usrId char(36), chName varchar(35), prgStart timestamp, prgEnd timestamp, prgName varchar(120), tw real) RETURNS integer AS $$
DECLARE
	ret integer;
BEGIN
	IF usrId IS NOT NULL AND chName IS NOT NULL AND prgStart IS NOT NULL AND prgEnd IS NOT NULL AND prgName IS NOT NULL AND tw IS NOT NULL THEN
		IF EXISTS (SELECT * FROM ProgrammesUsersTTW WHERE ProgrammesUsersTTW.userId = usrId AND ProgrammesUsersTTW.channelName = chName AND ProgrammesUsersTTW.programmeStart = prgStart AND ProgrammesUsersTTW.programmeEnd = prgEnd AND ProgrammesUsersTTW.programmeName = prgName) THEN
			UPDATE ProgrammesUsersTTW SET programmeEnd = prgEnd, programmeName = prgName, timeWatched = tw WHERE ProgrammesUsersTTW.userId = usrId AND ProgrammesUsersTTW.channelName = chName AND ProgrammesUsersTTW.programmeStart = prgStart;
		ELSE
			INSERT INTO ProgrammesUsersTTW (userId, channelName, programmeStart, programmeEnd, programmeName, timeWatched) VALUES (usrId, chName, prgStart, prgEnd, prgName, tw);
		END IF;
		
		GET DIAGNOSTICS ret = ROW_COUNT;
		RETURN ret;
	ELSE 
		RETURN NULL;
	END IF;
END;
$$ LANGUAGE plpgsql;