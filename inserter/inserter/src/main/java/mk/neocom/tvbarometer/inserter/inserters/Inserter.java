package mk.neocom.tvbarometer.inserter.inserters;

import java.io.File;

public interface Inserter
{	
	void insertEmissions(File file) throws InserterException;
	void insertUserTTWStatistics(File file) throws InserterException;
}