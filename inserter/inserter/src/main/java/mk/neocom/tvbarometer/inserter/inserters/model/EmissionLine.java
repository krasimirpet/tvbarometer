package mk.neocom.tvbarometer.inserter.inserters.model;

import java.util.Date;

public class EmissionLine
{
	private String channel;
	private Date programmeStart;
	private Date programmeEnd;
	private String programme;
	
	public EmissionLine(){}
	
	public EmissionLine(String channel, Date programmeStart, Date programmeEnd, String programme)
	{
		this.channel = channel;
		this.programmeStart = programmeStart;
		this.programmeEnd = programmeEnd;
		this.programme = programme;
	}
	
	public String getChannel(){
		return channel;
	}
	
	public void setChannel(String channel){
		this.channel = channel;
	}
	
	public Date getProgrammeStart(){
		return programmeStart;
	}
	
	public void setProgrammeStart(Date programmeStart){
		this.programmeStart = programmeStart;
	}
	
	public Date getProgrammeEnd(){
		return programmeEnd;
	}
	
	public void setProgrammeEnd(Date programmeEnd){
		this.programmeEnd = programmeEnd;
	}
	
	public String getProgramme(){
		return programme;
	}
	
	public void setProgramme(String programme){
		this.programme = programme;
	}
}