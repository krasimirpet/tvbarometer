package mk.neocom.tvbarometer.inserter.inserters;

public class InserterException extends Exception
{
	public InserterException(){
		super();
	}
	
	public InserterException(String message)
	{
		super(message);
	}
}