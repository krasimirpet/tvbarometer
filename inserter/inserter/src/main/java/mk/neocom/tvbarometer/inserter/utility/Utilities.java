package mk.neocom.tvbarometer.inserter.utility;

import java.io.File;
import java.io.FilenameFilter;

public class Utilities {
	public static File[] getFilesInDirectoryWithExtension(File directory, String extension)
	{
		final String finalExtension = extension;
		
		return directory.listFiles(new FilenameFilter(){
	        public boolean accept(File dir, String filename)
			{
				return filename.endsWith("." + finalExtension);
			}
		});
	}
}