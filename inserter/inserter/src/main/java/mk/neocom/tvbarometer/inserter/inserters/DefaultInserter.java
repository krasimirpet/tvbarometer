package mk.neocom.tvbarometer.inserter.inserters;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import mk.neocom.tvbarometer.inserter.inserters.model.EmissionLine;
import mk.neocom.tvbarometer.inserter.inserters.model.User;
import mk.neocom.tvbarometer.inserter.inserters.model.UserTTWStatLine;

public class DefaultInserter {
	protected static class HeaderConstants {
		public static final String USERID = "userId";
		public static final String ACCOUNTID = "accountId";
		public static final String REFNUMBER = "refNumber";
		public static final String FIRSTNAME = "firstName";
		public static final String LASTNAME = "lastName";
		public static final String USERNAME = "username";
		
		public static final String CHANNEL = "channel";
		public static final String PROGRAMMERECORD = "programme emission";
		public static final String PROGRAMMERECORD_START = "programme start";
		public static final String PROGRAMMERECORD_END = "programme end";
		public static final String TIME_MINUTES = "time (minutes)";
	}
	
	protected static final char STAT_TEXT_DELIMITER = '"';
	
	protected Set<String> usersConfirmedInDatabase = new HashSet<String>();
	
	protected Connection dbConnection;
	
	protected DefaultInserter(Connection dbConnection){
		this.dbConnection = dbConnection;
	}
	
	protected static List<EmissionLine> parseEmissions(File file) throws IOException
	{
		List<String> lines = Files.readAllLines(file.toPath(), Charset.forName("UTF-8"));
		Iterator<String> iterLines = lines.iterator();
		
		if(iterLines.hasNext()){
			//read header
			List<String> headerParts = DefaultInserter.splitLine(iterLines.next());
			final int headerPartsSize = headerParts.size();
			
			int indexChannel = -1, indexProgrammeStart = -1, indexProgrammeEnd = -1, indexProgrammeRecord = -1;
			for(int i = 0; i < headerPartsSize; i++){
				String normalizedHeaderPart = DefaultInserter.normalizeLinePart(headerParts.get(i));
				
				if(normalizedHeaderPart.equals(HeaderConstants.CHANNEL)){
					indexChannel = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.PROGRAMMERECORD_START)){
					indexProgrammeStart = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.PROGRAMMERECORD_END)){
					indexProgrammeEnd = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.PROGRAMMERECORD)){
					indexProgrammeRecord = i;
				}
			}
				
			if(indexChannel == -1 || indexProgrammeStart == -1 || indexProgrammeEnd == -1 || indexProgrammeRecord == -1){
				//invalid file
				return null;
			}
			
			//read lines
			List<EmissionLine> linesInMemory = new ArrayList<EmissionLine>(lines.size());
			while(iterLines.hasNext()){
				try{
					List<String> lineParts = DefaultInserter.splitLine(iterLines.next());
					if(lineParts.size() != headerPartsSize){
						continue;
					}
					
					String channel = DefaultInserter.normalizeLinePart(lineParts.get(indexChannel));
					Date programmeStart = DefaultInserter.parseDateTimeTimestamp(DefaultInserter.normalizeLinePart(lineParts.get(indexProgrammeStart)));
					Date programmeEnd = DefaultInserter.parseDateTimeTimestamp(DefaultInserter.normalizeLinePart(lineParts.get(indexProgrammeEnd)));
					String programmeRecord = DefaultInserter.normalizeLinePart(lineParts.get(indexProgrammeRecord));
					
					linesInMemory.add(new EmissionLine(channel, programmeStart, programmeEnd, programmeRecord));
				}catch(IllegalArgumentException e){}	//skip the line
			}
			
			return linesInMemory;
		}else{
			return null;
		}
	}
	
	protected static List<UserTTWStatLine> parseUserTTWStatistics(File file) throws IOException
	{
		List<String> lines = Files.readAllLines(file.toPath(), Charset.forName("UTF-8"));
		Iterator<String> iterLines = lines.iterator();
		
		if(iterLines.hasNext()){
			//read header
			List<String> headerParts = DefaultInserter.splitLine(iterLines.next());
			final int headerPartsSize = headerParts.size();
			
			int indexUserId = -1, indexAccountId = -1, indexRefNumber = -1, indexFirstName = -1, indexLastName = -1, indexUsername = -1, indexChannel = -1, indexProgrammeStart = -1, indexProgrammeEnd = -1, indexProgrammeRecord = -1, indexTime = -1;
			for(int i = 0; i < headerPartsSize; i++){
				String normalizedHeaderPart = DefaultInserter.normalizeLinePart(headerParts.get(i));
				
				if(normalizedHeaderPart.equals(HeaderConstants.USERID)){
					indexUserId = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.ACCOUNTID)){
					indexAccountId = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.REFNUMBER)){
					indexRefNumber = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.FIRSTNAME)){
					indexFirstName = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.LASTNAME)){
					indexLastName = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.USERNAME)){
					indexUsername = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.CHANNEL)){
					indexChannel = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.PROGRAMMERECORD_START)){
					indexProgrammeStart = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.PROGRAMMERECORD_END)){
					indexProgrammeEnd = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.PROGRAMMERECORD)){
					indexProgrammeRecord = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.TIME_MINUTES)){
					indexTime = i;
				}
			}
				
			if(indexUserId == -1 || indexAccountId == -1 || indexRefNumber == -1 || indexFirstName == -1 || indexLastName == -1 || indexUsername == -1 || indexChannel == -1 || indexProgrammeStart == -1 || indexProgrammeEnd == -1 || indexProgrammeRecord == -1 || indexTime == -1){
				//invalid file
				return null;
			}
			
			//read lines
			List<UserTTWStatLine> linesInMemory = new ArrayList<UserTTWStatLine>(lines.size());
			while(iterLines.hasNext()){
				try{
					List<String> lineParts = DefaultInserter.splitLine(iterLines.next());
					if(lineParts.size() != headerPartsSize){
						continue;
					}
					
					String userId = DefaultInserter.normalizeLinePart(lineParts.get(indexUserId));
					String accountId = DefaultInserter.normalizeLinePart(lineParts.get(indexAccountId));
					String refNumber = DefaultInserter.normalizeLinePart(lineParts.get(indexRefNumber));
					String firstName = DefaultInserter.normalizeLinePart(lineParts.get(indexFirstName));
					String lastName = DefaultInserter.normalizeLinePart(lineParts.get(indexLastName));
					String username = DefaultInserter.normalizeLinePart(lineParts.get(indexUsername));
					
					User user = new User(userId, accountId, refNumber, firstName, lastName, username);
					
					String channel = DefaultInserter.normalizeLinePart(lineParts.get(indexChannel));
					Date programmeStart = DefaultInserter.parseDateTimeTimestamp(DefaultInserter.normalizeLinePart(lineParts.get(indexProgrammeStart)));
					Date programmeEnd = DefaultInserter.parseDateTimeTimestamp(DefaultInserter.normalizeLinePart(lineParts.get(indexProgrammeEnd)));
					String programmeRecord = DefaultInserter.normalizeLinePart(lineParts.get(indexProgrammeRecord));
					float time = Float.parseFloat(DefaultInserter.normalizeLinePart(lineParts.get(indexTime)));
					
					linesInMemory.add(new UserTTWStatLine(user, channel, programmeStart, programmeEnd, programmeRecord, time));
				}catch(IllegalArgumentException e){}	//skip the line
			}
			
			return linesInMemory;
		}else{
			return null;
		}
	}
	
	protected void insertUserIfNecessary(User user) throws SQLException, InserterException {
		if(usersConfirmedInDatabase.contains(user.getUserId())){
			//user exists in database
			return;
		}else{
			//check if user exists in database, and if not, insert the user
			String userId = user.getUserId();
			
			PreparedStatement selectUser = dbConnection.prepareStatement("SELECT * FROM Users WHERE Users.userId = ?");
			selectUser.setString(1, userId);
			
			ResultSet results = selectUser.executeQuery();
			boolean resultsNotEmpty = !results.next();
			
			results.close();
			selectUser.close();
			
			if(resultsNotEmpty){
				//user does not exist in database, insert the user
				PreparedStatement insertUser = dbConnection.prepareStatement("INSERT INTO Users (userId, accountId, refNumber, firstName, lastName, username) VALUES (?, ?, ?, ?, ?, ?)");
				insertUser.setString(1, user.getUserId());
				insertUser.setString(2, user.getAccountId());
				insertUser.setString(3, user.getRefNumber());
				insertUser.setString(4, user.getFirstName());
				insertUser.setString(5, user.getLastName());
				insertUser.setString(6, user.getUsername());
				
				int rowcount = insertUser.executeUpdate();
				
				insertUser.close();
				
				if(rowcount != 1){
					throw new InserterException("Не може да се вметне нов корисник во базата на податоци.");
				}
			}
			
			usersConfirmedInDatabase.add(userId);
		}
	}
	
	protected static java.sql.Timestamp utilToSqlTimestamp(java.util.Date date){
		return new java.sql.Timestamp(date.getTime());
	}
	
	private static Date parseDateTimeTimestamp(String timestamp) throws IllegalArgumentException
	{
		try{
			return new SimpleDateFormat("dd MMM yyyy HH:mm:ss", Locale.ENGLISH).parse(timestamp);
		}catch(ParseException e){
			throw new IllegalArgumentException("invalid timestamp");
		}
	}
	
	private static List<String> splitLine(String line){
		List<String> parts = new ArrayList<String>(10);
		
		int length = line.length();
		int startIndex = 0;
		boolean inQuotes = false;
		for(int i = 0; i < length; i++){
			if(line.charAt(i) == DefaultInserter.STAT_TEXT_DELIMITER){
				inQuotes = !inQuotes;
			}else if(line.charAt(i) == ',' && !inQuotes){
				parts.add(line.substring(startIndex, i));
				startIndex = i + 1;
			}
		}
		parts.add(line.substring(startIndex, length));
		
		return parts;
	} 
	
	private static String normalizeLinePart(String linePart)
	{
		//trims quotes at the beginning and end of the line part
		boolean startsWithQuote = linePart.startsWith("\"");
		boolean endsWithQuote = linePart.endsWith("\"");
		
		if(startsWithQuote && endsWithQuote){
			linePart = linePart.substring(1, linePart.length() - 1);
		}else if(startsWithQuote){
			linePart = linePart.substring(1);
		}else if(endsWithQuote){
			linePart = linePart.substring(0, linePart.length() - 1);
		}
		
		return linePart;
	}
}