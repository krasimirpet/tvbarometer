package mk.neocom.tvbarometer.inserter.inserters.model;

import java.util.Date;

import mk.neocom.tvbarometer.inserter.inserters.model.User;

public class UserTTWStatLine
{
	protected User user;
	private String channel;
	private Date programmeStart;
	private Date programmeEnd;
	private String programme;
	private float time;	//in hours
	
	public UserTTWStatLine(){
		super();
	}
	
	public UserTTWStatLine(User user, String channel, Date programmeStart, Date programmeEnd, String programme, float time)
	{
		this.user = user;
		this.channel = channel;
		this.programmeStart = programmeStart;
		this.programmeEnd = programmeEnd;
		this.programme = programme;
		this.time = time;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public void setUser(User user)
	{
		this.user = user;
	}
	
	public String getChannel(){
		return channel;
	}
	
	public void setChannel(String channel){
		this.channel = channel;
	}
	
	public Date getProgrammeStart(){
		return programmeStart;
	}
	
	public void setProgrammeStart(Date programmeStart){
		this.programmeStart = programmeStart;
	}
	
	public Date getProgrammeEnd(){
		return programmeEnd;
	}
	
	public void setProgrammeEnd(Date programmeEnd){
		this.programmeEnd = programmeEnd;
	}
	
	public String getProgramme(){
		return programme;
	}
	
	public void setProgramme(String programme){
		this.programme = programme;
	}
	
	public float getTime(){
		return time;
	}
	
	public void setTime(float time){
		this.time = time;
	}
}