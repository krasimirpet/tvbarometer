package mk.neocom.tvbarometer.inserter;

public class InserterAppException extends Exception
{
	public InserterAppException(){
		super();
	}
	
	public InserterAppException(String message)
	{
		super(message);
	}
}