package mk.neocom.tvbarometer.inserter.logger;

public interface Logger{
	void logProgress(int processed, int total);
	void logStatus(String status);
}