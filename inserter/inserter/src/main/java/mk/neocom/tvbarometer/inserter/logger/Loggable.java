package mk.neocom.tvbarometer.inserter.logger;

public interface Loggable {
	void logProgress(int processed, int total);
	void logStatus(String status);
}