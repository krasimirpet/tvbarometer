package mk.neocom.tvbarometer.inserter;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JSpinner;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JProgressBar;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingUtilities;

import mk.neocom.tvbarometer.inserter.logger.Loggable;
import mk.neocom.tvbarometer.inserter.logger.Logger;
import mk.neocom.tvbarometer.inserter.logger.DefaultLogger;

import mk.neocom.tvbarometer.inserter.utility.Utilities;

import mk.neocom.tvbarometer.inserter.inserters.Inserter;
import mk.neocom.tvbarometer.inserter.inserters.InserterException;
import mk.neocom.tvbarometer.inserter.inserters.DefaultInserterImpl;

public class InserterApp
{
	private static final int CONNECTION_TIMEOUT = 10;
	private static final String USER_STATISTICS_DIRECTORY_NAME = "users";
	
	private static final String DEFAULT_DBHOST = "127.0.0.1";
	private static final int DEFAULT_DBPORT = 5432;
	private static final String DEFAULT_DBNAME = "tvbarometer";
	private static final int DEFAULT_BATCH_SIZE = 50;
	
	private static boolean insertingInProgress = false;
	private static Thread workingThread;
	
	private static JTextField textStat;
	private static JTextField textDBHost;
	private static JTextField textDBName;
	private static JTextField textDBUser;
	private static JPasswordField textDBPass;
	private static JSpinner spinnerDBPort;
	private static JSpinner spinnerBatchSize;
	private static JLabel labelStatus;
	private static JProgressBar progressBar;
	private static JButton buttonAbort;
	private static JFileChooser fcStat;
	
    public static void main(String[] args)
    {
    	//load the JDBC driver
		try{
			Class.forName("org.postgresql.Driver");
		}catch (ClassNotFoundException e){
			System.out.println("внатрешна грешка - не може да се вчита postgresql jdbc драјверот");
		}
		
    	if(args.length == 0){
    		//display UI
    		SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    InserterApp.setUpGUI();
                }
            });
    	}else if(args.length == 1 && args[0].toLowerCase().equals("-h")){
    		//print help
    		System.out.print(InserterApp.getUsage());
    	}else{
    		try{
    			//begin process based on command line parameters
	    		Object[] parameters = InserterApp.parseCommandLineParameters(args);
	    		
	    		File statDir = (File)parameters[1];
	    		
	    		String dbHost = (String)parameters[2];
	    		if(dbHost == null){
		        	dbHost = InserterApp.DEFAULT_DBHOST;
		        }
	    		
	    		Integer dbPort = (Integer)parameters[3];
	    		if(dbPort == null){
		        	dbPort = InserterApp.DEFAULT_DBPORT;
		        }
	    		
	    		String dbName = (String)parameters[4];
	    		if(dbName == null){
		        	dbName = InserterApp.DEFAULT_DBNAME;
		        }
	    		
	    		String dbUser = (String)parameters[5];
	    		String dbPass = (String)parameters[6];
	    		
	    		Integer batchSize = (Integer)parameters[7];
	    		if(batchSize == null){
		        	batchSize = InserterApp.DEFAULT_BATCH_SIZE;
		        }
	    		
	    		InserterApp.insertStatistics(statDir, dbHost, dbPort, dbName, dbUser, dbPass, batchSize);
    		}catch(InserterAppException exception){
    			System.out.println("Упатство за користење:");
    	        System.out.println();
    			System.out.println(exception.getMessage());
    			return;
    		}
    	}
    	
    	insertingInProgress = false;
    }
    
    private static void setUpGUI()
    {
        final JFrame frame = new JFrame("ТВ Барометар - Вметнувач");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JMenuBar menuBar = new JMenuBar();
        JMenu menuAbout = new JMenu("За апликацијата"); 
        
        JMenuItem menuItemInstructions = new JMenuItem("Инструкции за користење...");
        menuItemInstructions.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				JOptionPane.showMessageDialog(frame, "<html><body><div style='width:400px;text-align:justify !IMPORTANT;'>" + InserterApp.getUsage().replaceAll("\n", "<br />") + "</div></body></html>", "Инструкции за користење", JOptionPane.INFORMATION_MESSAGE);
			}
        });
        menuAbout.add(menuItemInstructions);
        
        menuBar.add(menuAbout);
        
        frame.setJMenuBar(menuBar);

        JPanel statPanel = new JPanel();
        statPanel.setBorder(BorderFactory.createTitledBorder("Поддесувања"));
        statPanel.setLayout(new GridLayout(1, 1));
        
        JPanel panelStatContainer = new JPanel();
        panelStatContainer.setLayout(new BoxLayout(panelStatContainer, BoxLayout.Y_AXIS));
        
        JPanel panelStat = new JPanel();
        panelStat.setLayout(new BoxLayout(panelStat, BoxLayout.X_AXIS));
        
        JLabel labelStat = new JLabel("Статистики:");
        labelStat.setAlignmentX(Component.LEFT_ALIGNMENT);
        
        textStat = new JTextField();
        textStat.setEditable(false);
        textStat.setMaximumSize(new Dimension(10000, 26));
        textStat.setBorder(BorderFactory.createCompoundBorder(textStat.getBorder(), BorderFactory.createEmptyBorder(0, 3, 0, 0)));
        JButton buttonLogs = new JButton("Избери...");
        buttonLogs.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				if(fcStat.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION){
					textStat.setText(fcStat.getSelectedFile().getPath());
				}
			}
        });
        fcStat = new JFileChooser();
        fcStat.setDialogTitle("Изберете го директориумот кој ги содржи статистиките:");
        fcStat.setMultiSelectionEnabled(false);
        fcStat.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fcStat.setAcceptAllFileFilterUsed(false);
        
        panelStat.add(Box.createHorizontalStrut(4));
        panelStat.add(labelStat);
        panelStat.add(Box.createHorizontalStrut(60));
        panelStat.add(textStat);
        panelStat.add(Box.createHorizontalStrut(2));
        panelStat.add(buttonLogs);
        
        //panelStatContainer.add(Box.createVerticalStrut(2));
        panelStatContainer.add(panelStat);
        panelStatContainer.add(Box.createVerticalStrut(3));
        
        statPanel.add(panelStatContainer);
        
        JPanel centerContainer = new JPanel();
        centerContainer.setLayout(new BoxLayout(centerContainer, BoxLayout.Y_AXIS));
        
        JPanel connectionPanel = new JPanel();
        connectionPanel.setBorder(BorderFactory.createTitledBorder("Поврзување со RDBMS"));
        connectionPanel.setLayout(new BoxLayout(connectionPanel, BoxLayout.Y_AXIS));
        
        JPanel panelDBHost = new JPanel();
        panelDBHost.setLayout(new BoxLayout(panelDBHost, BoxLayout.X_AXIS));
        
        JLabel labelDBHost = new JLabel("Хост:");
        labelDBHost.setAlignmentX(Component.LEFT_ALIGNMENT);
        
        textDBHost = new JTextField();
        textDBHost.setMaximumSize(new Dimension(10000, 26));
        textDBHost.setBorder(BorderFactory.createCompoundBorder(textDBHost.getBorder(), BorderFactory.createEmptyBorder(0, 3, 0, 0)));
        textDBHost.setText(InserterApp.DEFAULT_DBHOST);

        panelDBHost.add(Box.createHorizontalStrut(4));
        panelDBHost.add(labelDBHost);
        panelDBHost.add(Box.createHorizontalStrut(97));
        panelDBHost.add(textDBHost);
        
        JPanel panelDBPort = new JPanel();
        panelDBPort.setLayout(new BoxLayout(panelDBPort, BoxLayout.X_AXIS));
        
        JLabel labelDBPort = new JLabel("Порт:");
        labelDBPort.setAlignmentX(Component.LEFT_ALIGNMENT);
        
        spinnerDBPort = new JSpinner();
        spinnerDBPort.setMaximumSize(new Dimension(80, 26));
        spinnerDBPort.setModel(new SpinnerNumberModel(InserterApp.DEFAULT_DBPORT, 1, 100000, 1));
        spinnerDBPort.setEditor(new JSpinner.NumberEditor(spinnerDBPort, "#"));	//the font being bold is a java bug: http://bugs.java.com/bugdatabase/view_bug.do?bug_id=6421058
        
        panelDBPort.add(Box.createHorizontalStrut(4));
        panelDBPort.add(labelDBPort);
        panelDBPort.add(Box.createHorizontalStrut(96));
        panelDBPort.add(spinnerDBPort);
        panelDBPort.add(Box.createHorizontalGlue());
        
        JPanel panelDBName = new JPanel();
        panelDBName.setLayout(new BoxLayout(panelDBName, BoxLayout.X_AXIS));
        
        JLabel labelDBName = new JLabel("База на податоци:");
        labelDBName.setAlignmentX(Component.LEFT_ALIGNMENT);
        
        textDBName = new JTextField();
        textDBName.setMaximumSize(new Dimension(10000, 26));
        textDBName.setBorder(BorderFactory.createCompoundBorder(textDBName.getBorder(), BorderFactory.createEmptyBorder(0, 3, 0, 0)));
        textDBName.setText(InserterApp.DEFAULT_DBNAME);

        panelDBName.add(Box.createHorizontalStrut(4));
        panelDBName.add(labelDBName);
        panelDBName.add(Box.createHorizontalStrut(21));
        panelDBName.add(textDBName);
        
        JPanel panelDBUser = new JPanel();
        panelDBUser.setLayout(new BoxLayout(panelDBUser, BoxLayout.X_AXIS));
        
        JLabel labelDBUser = new JLabel("Корисничко име:");
        labelDBUser.setAlignmentX(Component.LEFT_ALIGNMENT);
        
        textDBUser = new JTextField();
        textDBUser.setMaximumSize(new Dimension(10000, 26));
        textDBUser.setBorder(BorderFactory.createCompoundBorder(textDBUser.getBorder(), BorderFactory.createEmptyBorder(0, 3, 0, 0)));
        textDBUser.setText("postgres");	//this default is only for the UI

        panelDBUser.add(Box.createHorizontalStrut(4));
        panelDBUser.add(labelDBUser);
        panelDBUser.add(Box.createHorizontalStrut(30));
        panelDBUser.add(textDBUser);
        
        JPanel panelDBPass = new JPanel();
        panelDBPass.setLayout(new BoxLayout(panelDBPass, BoxLayout.X_AXIS));
        
        JLabel labelDBPass = new JLabel("Лозинка:");
        labelDBPass.setAlignmentX(Component.LEFT_ALIGNMENT);
        
        textDBPass = new JPasswordField();
        textDBPass.setMaximumSize(new Dimension(10000, 26));
        textDBPass.setBorder(BorderFactory.createCompoundBorder(textDBPass.getBorder(), BorderFactory.createEmptyBorder(0, 3, 0, 0)));
        
        panelDBPass.add(Box.createHorizontalStrut(4));
        panelDBPass.add(labelDBPass);
        panelDBPass.add(Box.createHorizontalStrut(76));
        panelDBPass.add(textDBPass);
        
        connectionPanel.add(panelDBHost);
        connectionPanel.add(panelDBPort);
        connectionPanel.add(panelDBName);
        connectionPanel.add(panelDBUser);
        connectionPanel.add(panelDBPass);
        
        JPanel configurationPanel = new JPanel();
        configurationPanel.setBorder(BorderFactory.createTitledBorder("Поддесувања"));
        configurationPanel.setLayout(new BoxLayout(configurationPanel, BoxLayout.Y_AXIS));
        
        JPanel panelBatchSize = new JPanel();
        panelBatchSize.setLayout(new BoxLayout(panelBatchSize, BoxLayout.X_AXIS));
        
        JLabel labelBatchSize = new JLabel("Големина на пакет:");
        labelBatchSize.setAlignmentX(Component.LEFT_ALIGNMENT);
        
        spinnerBatchSize = new JSpinner();
        spinnerBatchSize.setMaximumSize(new Dimension(80, 26));
        spinnerBatchSize.setModel(new SpinnerNumberModel(InserterApp.DEFAULT_BATCH_SIZE, 1, 100000, 1));
        spinnerBatchSize.setEditor(new JSpinner.NumberEditor(spinnerBatchSize, "#"));
        
        panelBatchSize.add(Box.createHorizontalStrut(4));
        panelBatchSize.add(labelBatchSize);
        panelBatchSize.add(Box.createHorizontalStrut(14));
        panelBatchSize.add(spinnerBatchSize);
        panelBatchSize.add(Box.createHorizontalGlue());
        
        Component configurationSizeHolder = new Component(){};
        configurationSizeHolder.setMaximumSize(new Dimension(10000, 1));
        configurationSizeHolder.setVisible(true);
        
        configurationPanel.add(panelBatchSize);
        configurationPanel.add(configurationSizeHolder);
        
        JPanel statusPanel = new JPanel();
        statusPanel.setBorder(BorderFactory.createTitledBorder("Статус"));
        statusPanel.setLayout(new GridLayout(2, 1));
        
        JPanel labelStatusContainer = new JPanel();
        labelStatusContainer.setLayout(new BoxLayout(labelStatusContainer, BoxLayout.X_AXIS));
        
        labelStatus = new JLabel("Операција: нема извршување");
        labelStatus.setAlignmentX(Component.LEFT_ALIGNMENT);
        
        labelStatusContainer.add(Box.createHorizontalStrut(4));
        labelStatusContainer.add(labelStatus);
        labelStatusContainer.add(Box.createHorizontalGlue());
        
        JPanel progressBarContainer = new JPanel();
        progressBarContainer.setLayout(new BoxLayout(progressBarContainer, BoxLayout.X_AXIS));
        
        progressBar = new JProgressBar(JProgressBar.HORIZONTAL, 0, 100);
        progressBar.setValue(0);
        
        progressBarContainer.add(Box.createHorizontalStrut(4));
        progressBarContainer.add(progressBar);
        progressBarContainer.add(Box.createHorizontalStrut(1));
        
        statusPanel.add(labelStatusContainer);
        statusPanel.add(progressBarContainer);
        
        centerContainer.add(connectionPanel);
        centerContainer.add(configurationPanel);
        centerContainer.add(statusPanel);
        
        JPanel actionPanel = new JPanel();
        actionPanel.setBorder(BorderFactory.createTitledBorder("Извршување"));
        
        actionPanel.setLayout(new GridLayout(1, 1));
        
        JPanel actionButtonsContainer = new JPanel();
        actionButtonsContainer.setLayout(new BoxLayout(actionButtonsContainer, BoxLayout.X_AXIS));
        
        JButton executeButton = new JButton("Вметни");
        executeButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				if(!insertingInProgress){
					buttonAbort.setVisible(true);
					
					final File statDir = fcStat.getSelectedFile();
					
					final String dbHost = textDBHost.getText();
					final int dbPort = ((Number)spinnerDBPort.getValue()).intValue();
					final String dbName = textDBName.getText();
					final String dbUser = textDBUser.getText();
					final String dbPass = new String(textDBPass.getPassword());
					final int batchSize = ((Number)spinnerBatchSize.getValue()).intValue();
					
					if(statDir == null){
						JOptionPane.showMessageDialog(frame, "<html><body><div style='width:360px;text-align:justify !IMPORTANT;'>" + "Изберете го директориумот каде што се наоѓаат статистиките генерирани од парсерот." + "</div></body></html>", "Грешка", JOptionPane.ERROR_MESSAGE);
					}else if(dbHost.isEmpty()){
						JOptionPane.showMessageDialog(frame, "<html><body><div style='width:360px;text-align:justify !IMPORTANT;'>" + "Внесете ја адресата на серверот на базата на податоци." + "</div></body></html>", "Грешка", JOptionPane.ERROR_MESSAGE);
					}else if(dbName.isEmpty()){
						JOptionPane.showMessageDialog(frame, "<html><body><div style='width:360px;text-align:justify !IMPORTANT;'>" + "Внесете го името на базата на податоци." + "</div></body></html>", "Грешка", JOptionPane.ERROR_MESSAGE);
					}else if(dbUser.isEmpty()){
						JOptionPane.showMessageDialog(frame, "<html><body><div style='width:360px;text-align:justify !IMPORTANT;'>" + "Внесете го корисничкото име на сметката за базата на податоци." + "</div></body></html>", "Грешка", JOptionPane.ERROR_MESSAGE);
					}else if(batchSize < 1 || batchSize > 10000){
						JOptionPane.showMessageDialog(frame, "<html><body><div style='width:360px;text-align:justify !IMPORTANT;'>" + "Големината на пакетот записи кои се вметнуваат истовремено мора да биде во интервалот [1, 10000]." + "</div></body></html>", "Грешка", JOptionPane.ERROR_MESSAGE);
					}else{
						SwingUtilities.invokeLater(new Runnable(){
							public void run() {
								InserterApp.insertStatistics(statDir, dbHost, dbPort, dbName, dbUser, dbPass, batchSize);
							}
						});
					}
				}else{
					SwingUtilities.invokeLater(new Runnable(){
						public void run() {
							JOptionPane.showMessageDialog(frame, "<html><body><div style='width:290px;text-align:justify !IMPORTANT;'>" + "Потребно е тековното вметнување да заврши пред да се започне со ново." + "</div></body></html>", "Грешка", JOptionPane.ERROR_MESSAGE);
						}
					});
				}
			}
        });
        
        buttonAbort = new JButton("Прекини");
        buttonAbort.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				if(insertingInProgress){
					workingThread.interrupt();
				}
			}
        });
        buttonAbort.setVisible(false);
        
        JButton resetButton = new JButton("Ресетирај");
        resetButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				fcStat.setSelectedFile(null);
				textStat.setText("");
				
				progressBar.setValue(0);
				labelStatus.setText("Операција: нема извршување");
			}
        });
        
        actionButtonsContainer.add(Box.createHorizontalGlue());
        actionButtonsContainer.add(executeButton);
        actionButtonsContainer.add(Box.createHorizontalStrut(5));
        actionButtonsContainer.add(resetButton);
        actionButtonsContainer.add(Box.createHorizontalGlue());
        
        actionPanel.add(actionButtonsContainer);
        
        Container contentPane = frame.getContentPane();
        contentPane.setLayout(new BorderLayout());
        
        contentPane.add(statPanel, BorderLayout.NORTH);
        contentPane.add(centerContainer);
        contentPane.add(actionPanel, BorderLayout.SOUTH);

        frame.setSize(new Dimension(820, 450));
        frame.setVisible(true);
    }
    
    private static void insertStatistics(File statDir, String dbHost, Integer dbPort, String dbName, String dbUser, String dbPass, Integer batchSize)
    {
    	insertingInProgress = true;
	    	
    	final File finalStatDir = statDir;
    	final String finalDBHost = dbHost;
    	final int finalDBPort = dbPort;
    	final String finalDBName = dbName;
    	final String finalDBUser = dbUser;
    	final String finalDBPass = dbPass;
    	final int finalBatchSize = batchSize;
    	
    	workingThread = new Thread(new Runnable(){
			public void run(){
				Logger logger = new DefaultLogger(new Loggable(){
					private String status;
					private float progress;
					
					public void logProgress(int processed, int total) {
						this.progress = 100 * (processed / (float)total);
						
						if(InserterApp.hasBuiltGUI()){
							final float finalProgress = this.progress;
							SwingUtilities.invokeLater(new Runnable(){
								public void run(){
									progressBar.setValue(Math.round(finalProgress));
								}
							});
						}else{
							System.out.print("\r" + String.format("%-40s", generateStatusForCMD()));
						}
					}
					
					public void logStatus(String status) {
						this.status = "Операција: " + status;
						
						if(labelStatus != null){
							final String finalStatus = this.status;
							SwingUtilities.invokeLater(new Runnable(){
								public void run(){
									labelStatus.setText(finalStatus);
								}
							});
						}else{
							System.out.print("\r" + String.format("%-40s", generateStatusForCMD()));
						}
					}
					
					private String generateStatusForCMD(){
						return this.status + " - " + String.format("%.2f", this.progress) + "%";
					}
				});
				
				Connection dbConnection = null;
				try{
					logger.logStatus("поврзување со базата на податоци");
					
					DriverManager.setLoginTimeout(InserterApp.CONNECTION_TIMEOUT);
					dbConnection = DriverManager.getConnection(String.format("jdbc:postgresql://%s:%d/%s", finalDBHost, finalDBPort, finalDBName), finalDBUser, finalDBPass);
					dbConnection.setAutoCommit(false);
				}catch(SQLException e){
					logger.logStatus("грешка во поврзување");
					if(InserterApp.hasBuiltGUI()){
						JOptionPane.showMessageDialog(null, "<html><body><div style='width:340px;text-align:justify !IMPORTANT;'>Не може да се оствари врска со базата на податоци! (SQLException:" + e.getMessage() + ")</div></body></html>", "Грешка", JOptionPane.ERROR_MESSAGE);
					}
				}
				
				if(dbConnection != null){
					DefaultInserterImpl i = new DefaultInserterImpl(dbConnection, finalBatchSize);
					i.setLogger(logger);
					
					Inserter inserter = i;
					
					File[] totalStatisticsFiles = Utilities.getFilesInDirectoryWithExtension(finalStatDir, "csv");
					File[] userStatisticsFiles = new File[0];
					
					File usersDirectory = new File(finalStatDir.getPath() + File.separator + InserterApp.USER_STATISTICS_DIRECTORY_NAME);
					if(usersDirectory.exists()){
						userStatisticsFiles = Utilities.getFilesInDirectoryWithExtension(usersDirectory, "csv");
					}
					
					boolean foundEmissions = false;
					boolean foundUserStatistics = false;
					for(File file : totalStatisticsFiles){
						if(!Thread.currentThread().isInterrupted()){
							if(file.getName().equals("emissions.csv")){
								try{
									foundEmissions = true;
									inserter.insertEmissions(file);
								}catch(InserterException e){
									logger.logStatus("грешка при вметнување");
									if(InserterApp.hasBuiltGUI()){
										JOptionPane.showMessageDialog(null, "<html><body><div style='width:340px;text-align:justify !IMPORTANT;'>" + e.getMessage().replaceAll("\n", "<br />") + "</div></body></html>", "Грешка при вметнување", JOptionPane.ERROR_MESSAGE);
									}
								}
							}
						}else{
							logger.logStatus("прекинато");
							break;
						}
					}
					
					for(File file : userStatisticsFiles){
						if(!Thread.currentThread().isInterrupted()){
							try{
								if(file.getName().endsWith("-stats.csv")){
									foundUserStatistics = true;
									inserter.insertUserTTWStatistics(file);
								}
							}catch(InserterException e){
								logger.logStatus("грешка при вметнување");
								if(InserterApp.hasBuiltGUI()){
									JOptionPane.showMessageDialog(null, "<html><body><div style='width:340px;text-align:justify !IMPORTANT;'>" + e.getMessage().replaceAll("\n", "<br />") + "</div></body></html>", "Грешка при вметнување", JOptionPane.ERROR_MESSAGE);
								}
							}
						}else{
							logger.logStatus("прекинато");
							break;
						}
					}
					
					if(foundEmissions || foundUserStatistics){
						try{
							logger.logStatus("завршување на трансакцијата");
							
							dbConnection.commit();
						}catch(SQLException e){
							logger.logStatus("грешка во трансакција");
							logger.logProgress(0, 1);
							
							if(InserterApp.hasBuiltGUI()){
								JOptionPane.showMessageDialog(null, "<html><body><div style='width:340px;text-align:justify !IMPORTANT;'>Не може да се заврши трансакцијата! (SQLException: " + e.getMessage() + ")</div></body></html>", "Грешка", JOptionPane.ERROR_MESSAGE);
							}
						}
						
						try{
							logger.logStatus("поништување на врската со базата на податоци");
							
							dbConnection.close();
							
							if(!Thread.currentThread().isInterrupted()){
								logger.logStatus("готово");
								logger.logProgress(0, 1);
							}
						}catch(SQLException e){
							logger.logStatus("грешка во поврзување");
							logger.logProgress(0, 1);
							
							if(InserterApp.hasBuiltGUI()){
								JOptionPane.showMessageDialog(null, "<html><body><div style='width:340px;text-align:justify !IMPORTANT;'>Не може да се поништи врската со базата на податоци! (SQLException: " + e.getMessage() + ")</div></body></html>", "Грешка", JOptionPane.ERROR_MESSAGE);
							}
						}
					}else{
						logger.logStatus("не постојат документи за вметнување");
						logger.logProgress(0, 1);
						
						if(InserterApp.hasBuiltGUI()){
							JOptionPane.showMessageDialog(null, "<html><body><div style='width:340px;text-align:justify !IMPORTANT;'>Не постојат соодветни документи за вметнување во наведениот директориум.</div></body></html>", "Грешка", JOptionPane.ERROR_MESSAGE);
						}
					}
				}
				
				insertingInProgress = false;
			}
    	});
    	workingThread.start();
    }
    
    private static Object[] parseCommandLineParameters(String[] args) throws InserterAppException
    {
        String statDirLocation = null;
        String dbHost = null;
        Integer dbPort = null;
        String dbName = null;
        String dbUser = null;
        String dbPass = null;
        Integer batchSize = null;
        
        boolean argsMode = false;
        int currentParam = -1; //0 is -stat, 1 is -dbhost, 2 is -dbport, 3 is -dbname, 4 is -dbuser, 5 is -dbpass, 6 is -batch
        for(int i = 0; i < args.length; i++)
        {
            if(!argsMode){
                //args[i] contains parameter
                if(statDirLocation == null && args[i].toLowerCase().equals("-stat")){
                	currentParam = 0;
                    argsMode = true;
                }else if(dbHost == null && args[i].toLowerCase().equals("-dbhost")){
                	currentParam = 1;
                    argsMode = true;
                }else if(dbPort == null && args[i].toLowerCase().equals("-dbport")){
                	currentParam = 2;
                    argsMode = true;
                }else if(dbName == null && args[i].toLowerCase().equals("-dbname")){
                	currentParam = 3;
                    argsMode = true;
                }else if(dbUser == null && args[i].toLowerCase().equals("-dbuser")){
                	currentParam = 4;
                    argsMode = true;
                }else if(dbPass == null && args[i].toLowerCase().equals("-dbpass")){
                	currentParam = 5;
                    argsMode = true;
                }else if(batchSize == null && args[i].toLowerCase().equals("-batch")){
                	currentParam = 6;
                    argsMode = true;
                }
            }else{
                //args[i] contains parameter value
                if(currentParam == 0){
                	statDirLocation = args[i];
                    argsMode = false;
                }else if(currentParam == 1){
                	dbHost = args[i];
                    argsMode = false;
                }else if(currentParam == 2){
                	try{
                		dbPort = Integer.parseInt(args[i]);
                	}catch(NumberFormatException exception){}
                	argsMode = false;
                }else if(currentParam == 3){
                	dbName = args[i];
                	argsMode = false;
                }else if(currentParam == 4){
                	dbUser = args[i];
                	argsMode = false;
                }else if(currentParam == 5){
                	dbPass = args[i];
                	argsMode = false;
                }else if(currentParam == 6){
                	try{
                		batchSize = Integer.parseInt(args[i]);
                	}catch(NumberFormatException exception){}
                	argsMode = false;
                }
            }
        }
        
        File statDir = new File(statDirLocation);
        
    	if(!statDir.isDirectory()){
    		throw new InserterAppException("Грешка во параметри на конзолна линија: -stat мора да посочува постоечки директориум.");
    	}else if(dbPort != null && (dbPort < 1 || dbPort > 100000)){
    		throw new InserterAppException("-dbport мора да претставува валиден порт за соодветниот комуникациски протокол.");
    	}else if(dbUser == null || dbUser.isEmpty()){
    		throw new InserterAppException("-dbuser мора да го содржи корисничкото име на сметката со која се пристапува до базата на податоци.");
    	}else if(batchSize != null && (batchSize < 1 && batchSize > 10000)){
    		throw new InserterAppException("Вредноста на -batch мора да биде во интервалот [1, 10000].");
    	}else{
    		return new Object[]{statDir, dbHost, dbPort, dbName, dbUser, dbPass, batchSize};
    	}
    }
    
    private static boolean hasBuiltGUI(){
    	return progressBar != null;
    }
    
    private static String getUsage()
    {
    	StringBuilder builder = new StringBuilder();
    	
        builder.append("Вметнувачот работи со PostgreSQL база на податоци и може да се користи преку графичкиот кориснички интерфејс, или од командна линија со следните параметри:");
        builder.append("\n\n");
        builder.append("-stat: локацијата на директориумот каде што се наоѓаат статистиките генерирани од парсерот.");
        builder.append("\n");
        builder.append("-dbhost: хост на кој работи базата на податоци. Доколку не е зададен, вредноста е " + InserterApp.DEFAULT_DBHOST + ".");
        builder.append("\n");
        builder.append("-dbport: порт на кој работи базата на податоци. Доколку не е зададен, вредноста е " + String.valueOf(InserterApp.DEFAULT_DBPORT) + ".");
        builder.append("\n");
        builder.append("-dbname: името на базата на податоци. Доколку не е зададен, вредноста е " + InserterApp.DEFAULT_DBNAME + ".");
        builder.append("\n");
        builder.append("-dbuser: корисничкото име на сметката со која се пристапува до базата на податоци.");
        builder.append("\n");
        builder.append("-dbpass: лозинката на сметката со која се пристапува до базата на податоци (доколку параметарот не е зададен, лозинката е празна).");
        builder.append("\n");
        builder.append("-batch: големина на пакетот записи кои се вметнуваат во базата наеднаш. Доколку параметарот не е зададен, неговата вредност е " + String.valueOf(InserterApp.DEFAULT_BATCH_SIZE) + ".");
        builder.append("\n");
        
        return builder.toString();
    }
}
