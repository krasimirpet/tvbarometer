package mk.neocom.tvbarometer.inserter.inserters;

import java.util.List;
import java.util.TimeZone;
import java.util.Calendar;
import java.io.File;
import java.io.IOException;

import java.sql.Connection;
import java.sql.CallableStatement;
import java.sql.SQLException;

import mk.neocom.tvbarometer.inserter.inserters.model.EmissionLine;
import mk.neocom.tvbarometer.inserter.inserters.model.User;
import mk.neocom.tvbarometer.inserter.inserters.model.UserTTWStatLine;
import mk.neocom.tvbarometer.inserter.logger.Logger;

public class DefaultInserterImpl extends DefaultInserter implements Inserter {
	
	protected int batchSize;
	
	protected Logger logger;
	
	public DefaultInserterImpl(Connection dbConnection, int batchSize){
		super(dbConnection);
		
		this.batchSize = batchSize;
	}
	
	public void insertEmissions(File file) throws InserterException {
		try{
			final String filename = file.getName();
			
			logger.logStatus("вчитување на " + filename);
			logger.logProgress(0, 1);
			
			List<EmissionLine> statistics = DefaultInserter.parseEmissions(file);
			if(statistics == null){
				return;
			}
			
			logger.logStatus("вметнување од " + filename);
			
			Calendar timezoneAdjustedCalendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Skopje"));
			
			//chName varchar(35), prgStart timestamp, prgEnd timestamp, prgName varchar(120)
			CallableStatement batchCallStatement = dbConnection.prepareCall("{call InsertOrUpdateEmission(?::varchar(35), ?::timestamp, ?::timestamp, ?::varchar(120))}");
			
			final int statisticsLength = statistics.size();
			for(int i = 0; i < statisticsLength; i += batchSize){
				final int upperBound = Math.min(i + batchSize, statisticsLength);
				for(int j = i; j < upperBound; j++){
					EmissionLine line = statistics.get(j);
					
					batchCallStatement.setString(1, line.getChannel());
					batchCallStatement.setTimestamp(2, DefaultInserter.utilToSqlTimestamp(line.getProgrammeStart()), timezoneAdjustedCalendar);
					batchCallStatement.setTimestamp(3, DefaultInserter.utilToSqlTimestamp(line.getProgrammeEnd()), timezoneAdjustedCalendar);
					batchCallStatement.setString(4, line.getProgramme());
					
					batchCallStatement.addBatch();
				}
				batchCallStatement.executeBatch();
				
				logger.logProgress(upperBound, statisticsLength);
			}
			
			logger.logStatus("завршување со " + filename);
			
			batchCallStatement.close();
		}catch(IOException e){
			throw new InserterException(String.format("Не може да се прочита документот %s (IOException: %s).", file.getName(), e.getMessage()));
		}catch(SQLException e){
			try{
				dbConnection.rollback();
			}catch(SQLException x){}
			throw new InserterException(String.format("Грешка во вметнувањето на записи во базата (SQLException: %s)", e.getMessage()));
		}
	}

	public void insertUserTTWStatistics(File file) throws InserterException {
		try{
			final String filename = file.getName();
			
			logger.logStatus("вчитување на " + filename);
			logger.logProgress(0, 1);
			
			List<UserTTWStatLine> statistics = DefaultInserter.parseUserTTWStatistics(file);
			if(statistics == null){
				return;
			}
			
			logger.logStatus("вметнување од " + filename);
			
			Calendar timezoneAdjustedCalendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Skopje"));
			
			//usrId char(36), chName varchar(35), prgStart timestamp, prgEnd timestamp, prgName varchar(120), tw real
			CallableStatement batchCallStatement = dbConnection.prepareCall("{call InsertOrUpdateUserTTW(?::char(36), ?::varchar(35), ?::timestamp, ?::timestamp, ?::varchar(120), ?::real)}");
			
			final int statisticsLength = statistics.size();
			for(int i = 0; i < statisticsLength; i += batchSize){
				final int upperBound = Math.min(i + batchSize, statisticsLength);
				for(int j = i; j < upperBound; j++){
					UserTTWStatLine line = statistics.get(j);
					
					User user = line.getUser();
					super.insertUserIfNecessary(user);
					
					batchCallStatement.setString(1, user.getUserId());
					batchCallStatement.setString(2, line.getChannel());
					batchCallStatement.setTimestamp(3, DefaultInserter.utilToSqlTimestamp(line.getProgrammeStart()), timezoneAdjustedCalendar);
					batchCallStatement.setTimestamp(4, DefaultInserter.utilToSqlTimestamp(line.getProgrammeEnd()), timezoneAdjustedCalendar);
					batchCallStatement.setString(5, line.getProgramme());
					batchCallStatement.setFloat(6, line.getTime());
					
					batchCallStatement.addBatch();
				}
				batchCallStatement.executeBatch();
				
				logger.logProgress(upperBound, statisticsLength);
			}
			
			logger.logStatus("завршување со " + filename);
			
			batchCallStatement.close();
		}catch(IOException e){
			throw new InserterException(String.format("Не може да се прочита документот %s (IOException: %s).", file.getName(), e.getMessage()));
		}catch(SQLException e){
			try{
				dbConnection.rollback();
			}catch(SQLException x){}
			throw new InserterException(String.format("Грешка во вметнувањето на записи во базата (SQLException: %s)", e.getMessage()));
		}
	}

	public void setLogger(Logger logger) {
		this.logger = logger;
	}
}