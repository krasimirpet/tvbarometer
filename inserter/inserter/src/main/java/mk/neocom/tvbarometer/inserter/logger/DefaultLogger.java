package mk.neocom.tvbarometer.inserter.logger;

public class DefaultLogger implements Logger {
	private Loggable output;
	
	public DefaultLogger(Loggable output){
		this.output = output;
	}
	
	public void logProgress(int processed, int total){			
		this.output.logProgress(processed, total);
	}
	
	public void logStatus(String status){			
		this.output.logStatus(status);
	}
}