<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<meta name="robots" content="noindex, nofollow" />
<title>Барометар на гледаност на ТВ програми</title>

<!-- favicon -->
<link rel="shortcut icon" href="app/images/favicon.ico" type="image/x-icon" />

<!-- Stylesheets -->
<link rel="stylesheet" href="app/styles/bootstrap.min.css" />
<link rel="stylesheet" href="app/styles/bootstrap-theme.min.css" />
<link rel="stylesheet" href="app/styles/angular-material.min.css" />
<link rel="stylesheet" href="app/styles/index.css" />

<!-- jQuery proper, Angular bootstraps it automatically -->
<script src="app/components/jquery.min.js"></script>

<!-- D3.js -->
<script src="app/components/d3.min.js"></script>

<!-- Bootstrap -->
<!--<script src="app/components/bootstrap.min.js"></script>-->

<!-- AngularJS scripts -->
<script src="app/components/angular.min.js"></script>
<script src="app/components/angular-animate.min.js"></script>
<script src="app/components/angular-aria.min.js"></script>
<script src="app/components/angular-messages.min.js"></script>
<script src="app/components/angular-material.min.js"></script>
<script src="app/components/angular-route.min.js"></script>
<script src="app/components/angular-resource.min.js"></script>
<script src="app/components/angular-ui-router.min.js"></script>
<script src="app/components/ui-router-styles.js"></script>
<script src="app/components/FileSaver.min.js"></script>
<script src="app/components/jszip.min.js"></script>
<script src="app/components/rgbcolor.js"></script>
<script src="app/components/StackBlur.js"></script>
<script src="app/components/canvg.min.js"></script>

<!-- The definition and the configuration of the application module -->
<script src="app/scripts/app.js"></script>

<!-- Services -->
<script src="app/scripts/services/ChartService.js"></script>

<!-- Routes -->
<script src="app/scripts/router.js"></script>

<!-- Controllers -->
<script src="app/scripts/controllers/chart.controller.js"></script>
<script src="app/scripts/controllers/usage.controller.js"></script>

</head>
<body ng-app="tvbarometerApp" ui-router-styles>
	<div class="outer-container">
		<div class="header">
			<h1>Барометар на гледаност на ТВ програми</h1>
			<hr />
			<div class="navigation">
				<a ui-sref="chart">Графици</a>
				<a ui-sref="usage">Користење на апликацијата</a>
			</div>
			<hr />
		</div>
	    <div class="partial-page-container">
	        <div ui-view></div>
	    </div>
	    <div class="footer">
		    <hr />
		    Сите права припаѓаат на Neocom & Neotel.<br />
		    Јули 2016
	    </div>
    </div>
</body>
</html>