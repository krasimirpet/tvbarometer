'use strict';

TVBarometerApp.factory('ChartService', ['$http', '$rootScope', function($http, $rootScope){
	var channels = null;
	var users = null;
	
	var formatDate = function(date){
		var formattedDay = (date.getDate() < 10 ? "0" : "") + date.getDate();
		var formattedMonth = (date.getMonth() + 1 < 10 ? "0" : "") + (date.getMonth() + 1);
		
		return formattedDay + "." + formattedMonth + "." + date.getFullYear();
	};
	
	var requestTransformationFunction = function(obj){
		var str = [];
	    for(var p in obj){
	    	str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
	    }
	    return str.join("&");
	};
	
	return {
		fetchStaticData: function(){
			//fetch channels
			var req = {
			   url: 'channels',
			   method: "GET",
			   isArray: true
		    };
				
			$http(req).then(function(response) {
				channels = response.data;
				
				$rootScope.$broadcast('staticDataFetch');
			}, function(response) {
				channels = null;
				
				$rootScope.$broadcast('staticDataFetch');
			});
			
			//fetch users
			var req = {
			   url: 'users',
			   method: "GET",
			   isArray: true
		    };
				
			$http(req).then(function(response) {
				users = response.data;
				
				$rootScope.$broadcast('staticDataFetch');
			}, function(response) {
				users = null;
				
				$rootScope.$broadcast('staticDataFetch');
			});
		},
		
		getChannelChart: function(channel, from, to, totalUsers){
			var reqBody =  {
				"channel": channel,
				"from": formatDate(from),
				"to": formatDate(to),
				"totalUsers": totalUsers
			};
			var req = {
				url: 'chart/total',
				method: "POST",
				transformRequest: requestTransformationFunction,
				data: reqBody,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		    };
			
			return $http(req);
		},
		
		getChannelChartForSingleUser: function(userid, channel, from, to){
			var reqBody = {
				"userid": userid,
				"channel": channel,
				"from": formatDate(from),
				"to": formatDate(to)
			};
			var req = {
				url: 'chart/user',
				method: "POST",
				transformRequest: requestTransformationFunction,
				data: reqBody,
				headers: { 'Content-Type': 'application/x-www-form-urlencoded' }
		    };
			
			return $http(req);
		},
		
		getChannels: function(){
			return channels ? channels.slice() : null;
		},
		
		getUsers: function(){
			return users ? users.slice() : null;
		}
	};
}]);