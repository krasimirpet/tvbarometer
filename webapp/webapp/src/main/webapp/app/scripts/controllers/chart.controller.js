'use strict';

TVBarometerApp.controller('ChartCtrl', ['$http', '$scope', '$state', '$rootScope', '$mdDialog', 'ChartService', 
    function ($http, $scope, $state, $rootScope, $mdDialog, ChartService) {
		var results;
		var totalCharts;
		
		var setCookie = function(cname, cvalue, exdays) {
		    var d = new Date();
		    d.setTime(d.getTime() + (exdays*24*60*60*1000));
		    var expires = "expires="+d.toUTCString();
		    document.cookie = cname + "=" + cvalue + "; " + expires;
		};

		var getCookie = function(cname) {
		    var name = cname + "=";
		    var ca = document.cookie.split(';');
		    for(var i = 0, len = ca.length; i < len; i++) {
		        var c = ca[i];
		        while (c.charAt(0) == ' ') {
		            c = c.substring(1);
		        }
		        if (c.indexOf(name) == 0) {
		            return c.substring(name.length, c.length);
		        }
		    }
		    return "";
		};
		
		$scope.inputErrors = [];
		$scope.executionInProgress = false;
		$scope.errors = [];
		$scope.downloadInProgress = false;
		
		$scope.channels = ["сите канали"];
		$scope.users = ["сите корисници"];
		$scope.filteredUsers = $scope.users.slice();
		
		$scope.selectedChannel = $scope.channels[0];
		$scope.selectedUser = $scope.filteredUsers[0];
		$scope.userFilter = "";
		
		var cookieFrom = getCookie("from");
		$scope.selectedPeriodFrom = cookieFrom != "" ? new Date(parseInt(cookieFrom)) : new Date();
		
		var cookieTo = getCookie("to");
		$scope.selectedPeriodTo = cookieTo != "" ? new Date(parseInt(cookieTo)) : new Date();
		
		$scope.selectedTotalUsers = getCookie("totalUsers");
		
		$scope.haveResults = false;
		$scope.resultItems = [];
		
		//init ui
		(function(){
			angular.element('input.total-users').css({
				"width": "50px",
				"margin-left": "5px",
				"margin-bottom": "-5px"
			});
		})();
		
		//month is [1, 12]
		var numberOfDaysInMonth = function(year, month){
		    var d = new Date(year, month, 0);
		    return d.getDate();
		};
		
		var getChartsPeriods = function(startDate, endDate){
			var results = [startDate];
			
			var currentDate = new Date(startDate.getTime());
			var doneProcessingFirst = false;
			do{
				if(!doneProcessingFirst){
					var daysInMonth = numberOfDaysInMonth(currentDate.getFullYear(), currentDate.getMonth() + 1);
					var monthEnd = new Date(currentDate.getFullYear(), currentDate.getMonth(), daysInMonth);
					if(monthEnd.getTime() < endDate.getTime()){
						results.push(monthEnd);
						currentDate = monthEnd;
					}else{
						break;
					}
					
					doneProcessingFirst = true;
				}else{
					var monthStart = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, 1);
					results.push(monthStart);
					
					var daysInMonth = numberOfDaysInMonth(currentDate.getFullYear(), currentDate.getMonth() + 2);
					var monthEnd = new Date(currentDate.getFullYear(), currentDate.getMonth() + 1, daysInMonth);
					if(monthEnd.getTime() < endDate.getTime()){
						results.push(monthEnd);
						currentDate = monthEnd;
					}else{
						break;
					}
				}
			}while(true);
			
			results.push(endDate);
			
			return results;
		};
		
		var parseDate = function(dateString){
			var dateParts = dateString.split(".");
			if(dateParts && dateParts.length == 3){
				var dayInMonth = dateParts[0];
				if(dayInMonth.startsWith("0")){
					dayInMonth = dayInMonth.substr(1);
				}
				var month = dateParts[1];
				if(month.startsWith("0")){
					month = month.substr(1);
				}
				var year = dateParts[2];
				
				return new Date(parseInt(year), parseInt(month) - 1, parseInt(dayInMonth));
			}else{
				return new Date(NaN);
			}
		};
		
		var processResults = function(){
			//remove null values
			for(var i = 0, len = results.length; i < len; i++){
			    if(results[i] == null){         
				    results.splice(i, 1);
				    len--;
				    i--;
			    }
			}
			
			if(results.length > 0){
				//need to sort results since requests do not necessarily finish in the order they were issued
				//ascending sort by period start, channel, and optionally, username
				results.sort(function(first, second){
					var firstFrom = parseDate(first.from);
					var secondFrom = parseDate(second.from);
					if(firstFrom.getTime() < secondFrom.getTime()){
						return -1;
					}else if(firstFrom.getTime() > secondFrom.getTime()){
						return 1;
					}else{
						if(first.channel < second.channel){
							return -1;
						}else if(first.channel > second.channel){
							return 1;
						}else{
							if(first.user && second.user){
								if(first.user.username < second.user.username){
									return -1;
								}else if(first.user.username > second.user.username){
									return 1;
								}else{
									return 0;
								}
							}else{
								return 0;
							}
						}
					}
				});
				
				$scope.resultItems.splice(0, $scope.resultItems.length);
				for(var i = 0, len = results.length; i < len; i++){
					 $scope.resultItems.push(getResultTitle(results[i]));
				}
				$scope.selectedResultIndex = 0;
				
				updateChartPreview();
				
				$scope.haveResults = true;
			}
		};
		
		var getResultTitle = function(result){
			return result.channel + (result.user ? " - " + result.user.username : "") + " (" + result.from + " - " + result.to + ")";
		};
		
		var updateChartPreview = function(){
			var image = generatePNG(results[$scope.selectedResultIndex]);
			
			var maxImgWidth = 800, maxImgHeight = 500;
			var imgWidth, imgHeight;
			
			if(image.width / image.height >= maxImgWidth / maxImgHeight){
				//image is wider than max size display image, scale by width
				imgWidth = Math.min(image.width, maxImgWidth);
				imgHeight = Math.round(imgWidth * image.height / image.width);
			}else{
				//image is taller than max size display image, scale by height
				imgHeight = Math.min(image.height, maxImgHeight);
				imgWidth =  Math.round(imgHeight * image.width / image.height);
			}
			
			angular.element("div.preview-container a.chart-preview-anchor").css({
				"width": imgWidth + "px",
				"height": imgHeight + "px"
			});
			
			angular.element("div.preview-container a.chart-preview-anchor img.chart-preview").attr({
				"src": "data:image/png;base64," + image.data,
				"width": imgWidth + "px",
				"height": imgHeight + "px"
			});
		};
		
		var generateJSON = function(result){
			var effectiveData = result.data;
			
			return JSON.stringify(effectiveData, null, "\t");
		};
		
		var generateCSV = function(result){
			var effectiveData = result.data;
			
			var result = "";
			var newline = "\r\n";
			
			//header
			result += "\"programmeName\",\"programmeStart\",\"programmeEnd\",\"popularity (percent)\"";
			
			//lines
			for(var i = 0, len = effectiveData.length; i < len; i++){
				var emission = effectiveData[i].programmeRecord;
				var popularity = effectiveData[i].value;
				
				result += newline;
				result += "\"" + emission.name + "\"," + emission.start + "," + emission.end + "," + popularity;
			}
			
			return result;
		};
		
		var generatePNG = function(result){
			var effectiveData = result.data;
			
			var includeDateStartInTitle = true;
			var getChartLabel;
			if(includeDateStartInTitle){
				getChartLabel = function(programmeRecord){
					return programmeRecord.name + " | " + programmeRecord.start.substring(0, 5) + " " + programmeRecord.start.substring(11, 16);
				};
			}else{
				getChartLabel = function(programmeRecord){
					return programmeRecord.name;
				};
			}
			
			//used for setting margins
			var longestProgrammeNameLength = effectiveData.length > 0 ? Math.max.apply(null, effectiveData.map(function(entry){ return getChartLabel(entry.programmeRecord).length; })) : 0;
			
			//generate title
			var title = ["Гледаност на емисии на " + result.channel];
			if(result.user){
				title[0] += " - " + result.user.firstName + " " + result.user.lastName + " (" + result.user.username + ")";
			}
			if(result.from != result.to){
				title.push(result.from + " - " + result.to);
			}else{
				title.push(result.from);
			}
			
			//set up dimensions
			var margin = {top: 110, right: 20, bottom: 20 + 7.25 * longestProgrammeNameLength, left: 60},
			    width = Math.max(Math.round(effectiveData.length * 15 + (effectiveData.length - 1) * 2), 1000),
			    height = 500,
			    elementWidth = width + margin.left + margin.right,
			    elementHeight = height + margin.top + margin.bottom;
			
			//create axes objects
			var paddingX = .4;
			var x = d3.scale.ordinal()
			    .rangeRoundBands([0, width], paddingX, 0);
	
			var y = d3.scale.linear()
			    .range([height, 0]);
	
			var xAxis = d3.svg.axis()
			    .scale(x)
			    .orient("bottom");
	
			var yAxis = d3.svg.axis()
			    .scale(y)
			    .orient("left")
			    .ticks(10, "%");
			
			//create svg element for chart
			var svg = d3.select("body").append("svg")
				.attr("id", "svg-chart")
				.attr("x", 0)
				.attr("y", 0)
			    .attr("width", elementWidth)
			    .attr("height", elementHeight);
			
			//ordinal scale sums values of duplicate keys, and programme emissions generally have non-unique names
			//therefore use element index as key to ensure uniqueness, then rename the generated labels 
			var indeces = new Array(effectiveData.length); 
			for(var i = 0, len = effectiveData.length; i < len; i++){
				indeces[i] = "i" + i;
			}
			x.domain(indeces);
			y.domain([0, 1]);
			
			//append axes
			svg.append("g")
			    .attr("class", "x axis")
			    .attr("transform", "translate(" + margin.left + "," + (margin.top + height) + ")")
			    .call(xAxis)
			  .selectAll("text")
				.attr("y", 0)
				.attr("x", 0)
				.attr("dx", "-10px")
				.attr("dy", ".35em")
				.attr("transform", "rotate(-90)")
				.style("text-anchor", "end");
	
			svg.append("g")
			    .attr("class", "y axis")
			    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			    .call(yAxis);
			
			//style axes
			svg.selectAll(".axis")
			  	.attr("font", "10px sans-serif");
			  
			svg.selectAll(".axis path, .axis line")
			    .style("fill", "none")
			    .style("stroke", "#000")
			    .attr("shape-rendering", "crispEdges");
			
			//draw horizontal lines for y-axis value orientation
			var dy = margin.top;
			var movementY = height / 10;
			for(var i = 0; i < 10; i++){
				var lineY = Math.round(dy + i * movementY);
				svg.append("g")
					 .append("line")
					  .attr("x1", margin.left)
					  .attr("y1", lineY)
					  .attr("x2", margin.left + width + 1)
					  .attr("y2", lineY)
					  .attr("stroke", "#ccc")
					  .attr("stroke-width", "1");
			}
			
			//draw bars
			svg.selectAll(".bar")
			    .data(indeces)
			  .enter().append("rect")
			    .attr("class", "bar")
			    .attr("transform", "translate(" + margin.left + "," + margin.top + ")")
			    .attr("x", function(entry){ return x(entry); })
			    .attr("width", x.rangeBand())
			    .attr("y", function(entry) { return y(effectiveData[parseInt(entry.substring(1))].value); })
			    .attr("height", function(entry) { return height - y(effectiveData[parseInt(entry.substring(1))].value); })
			    .style("fill", "steelblue");
			
			//rename labels
			svg.selectAll(".tick text").each(function(d,i){
					var element = angular.element(this);
					var elementText = element.text();
					if(elementText.startsWith("i")){
						var elementIndex = parseInt(elementText.substring(1));
						
						element.text(getChartLabel(effectiveData[elementIndex].programmeRecord));
					}
				});
			
			//draw delimiters for days if chart range spans more than one day 
			if(result.from != result.to){
				var currentDate;
				
				var x = margin.left;
				var yTop = margin.top;
				var yAxis = margin.top + height;
				
				var barWidthWithPadding = width / effectiveData.length;
				var halfMoveX = barWidthWithPadding / 2;
				
				var drawLine = function(g, x1, y1, x2, y2){
					g.append("line")
					  .attr("x1", x1)
					  .attr("y1", y1)
					  .attr("x2", x2)
					  .attr("y2", y2)
					  .attr("stroke", "#ccc")
					  .attr("stroke-width", "2");
				};
				var drawLabel = function(g, x, y, text){
					g.append("text")
					  .attr("x", x)
					  .attr("y", y)
					  .text(text)
					  .attr("fill", "#ccc")
					  .style("font-size", "12px");
				};
				
				for(var i = 0, len = effectiveData.length; i < len; i++){
					var currentRecord = effectiveData[i].programmeRecord;
					var recordStart = currentRecord.start.split(" ")[0];
					
					var endSplit = currentRecord.end.split(" ");
					var recordEnd = endSplit[0];
					var recordEndsOnMidnight = endSplit[1] == "00:00:00";	//midnight is the beginning of the next day, but here it should be treated as the end of the current day
					
					if(!currentDate){
						//first iteration
						//draw only label left (no need for line since it would overlap with y axis)
						var g = svg.append("g");
						drawLabel(g, margin.left + 4, margin.top + 13, recordStart.substring(0, 5));
					}else if(currentDate != recordStart){
						//draw line in between the two values (left of this record)
						var lineX = Math.round(margin.left + i * 2 * halfMoveX);
						
						var g = svg.append("g");
						drawLine(g, lineX, margin.top + 1, lineX, margin.top + height - 1);
						drawLabel(g, lineX + 4, margin.top + 13, recordStart.substring(0, 5));
					}
					
					if(recordStart != recordEnd && !recordEndsOnMidnight){ 	//second condition clause is to test whether record ends at midnight straight
						// draw line in the middle of record
						var lineX = Math.round(margin.left + (i * 2 + 1) * halfMoveX);
						
						var g = svg.append("g");
						drawLine(g, lineX, margin.top + 1, lineX, margin.top + height - 1);
						drawLabel(g, lineX + 4, margin.top + 13, recordEnd.substring(0, 5));
					}
					
					if(!recordEndsOnMidnight){
						currentDate = recordEnd;
					}else{
						currentDate = recordStart;
					}
				}
				/*
				//draw finishing line on the right of record
				if(effectiveData.length > 0){
					var lineX = Math.round(margin.left + effectiveData.length * 2 * halfMoveX);
					
					var g = svg.append("g");
					drawLine(g, lineX, margin.top, lineX, margin.top + height);
				}
				*/
			}
			
			//add title
			for(var i = 0, len = title.length; i < len; i++){
				  svg.append("text")
				    .attr("class", "title")
				    .attr("x", Math.round((width + margin.left + margin.right) / 2))
				    .attr("y", 40 + i * 30)
				    .style("text-anchor", "middle")
				    .style("text-align", "center")
				    .style("font-family", "sans-serif")
				    .style("font-size", "22px")
				    .text(title[i]);
			}
			
			//generate png
			var svgClientRect = svg[0][0].getBoundingClientRect();	//[0][0] accesses the DOM element in the object returned by d3.select()
			  
			var canvas = document.createElement('canvas');
			canvas.width = svgClientRect.width;
		    canvas.height = svgClientRect.height;
		    document.body.appendChild(canvas);
		      
			var ctx = canvas.getContext('2d');
			  
			//fill with background color
			ctx.save();
			ctx.beginPath();
			ctx.rect(0, 0, canvas.width, canvas.height);
			ctx.fillStyle = "white";
			ctx.fill();
			ctx.restore();
			  
			//draw the chart
			ctx.drawSvg("<svg>" + svg.html() + "</svg>", 0, 0, elementWidth, elementHeight);
			
			d3.select("svg#svg-chart").remove();
			  
			var imageData = canvas.toDataURL("image/png");
			imageData = imageData.substring(imageData.indexOf(",") + 1);
			  
			document.body.removeChild(canvas);
			  
			return {
				data: imageData,
				width: svgClientRect.width,
				height: svgClientRect.height
			};
		};
		
		var initiateBlobDownload = function(blob, filename){
			//using FileSaver.js
			saveAs(blob, filename);
		};
		
		$scope.downloadJSON = function(){
			var result = results[$scope.selectedResultIndex];
			
			//generate json
			var data = generateJSON(result);
			
			//create blob
			var blob = new Blob([data], {
				"type": "application/json;charset=utf-8"
			});
			
			//initiate download
			initiateBlobDownload(blob, getResultTitle(result) + ".json");
		};
		
		$scope.downloadCSV = function(){
			var result = results[$scope.selectedResultIndex];
			
			//generate csv
			var data = generateCSV(result);
			
			//create blob
			var blob = new Blob([data], {
				"type": "text/csv;charset=utf-8"
			});
			
			//initiate download
			initiateBlobDownload(blob, getResultTitle(result) + ".csv");
		};
		
		$scope.downloadPNG = function(){
			var result = results[$scope.selectedResultIndex];
			
			//get the data from the already generated image preview
			var imgData = angular.element("div.preview-container img.chart-preview").attr("src");
			var data = imgData.substring(imgData.indexOf(",") + 1);
			
			//create a blob
			var binStr = atob(data),
		    len = binStr.length,
		    arr = new Uint8Array(len);
			
			for (var i = 0; i < len; i++) {
			    arr[i] = binStr.charCodeAt(i);
			}
			
			var blob = new Blob([arr], {
				"type": "image/png;base64"
			});
			
			//initiate download
			initiateBlobDownload(blob, getResultTitle(result) + ".png");
		};
		
		$scope.downloadZIP = function(){
			if(!$scope.downloadInProgress){
				//generating the data for each chart takes a while
				//display progress bar
				$scope.downloadInProgress = true;
				
				var archiveAndDownload = function(arrayChartData, arrayJson, arrayCsv, arrayPng){
					//archive results and initiate download
					var archive = new JSZip();
					
					for(var i = 0, len = arrayChartData.length; i < len; i++){
						var title = getResultTitle(arrayChartData[i]);
						
						archive.file(title + ".json", arrayJson[i]);
						archive.file(title + ".csv", arrayCsv[i]);
						archive.file(title + ".png", arrayPng[i], {base64: true});
					}
					
					archive.generateAsync({type:"blob"}).then(function(blob){
					    initiateBlobDownload(blob, (arrayChartData.length == 1 ? "grafik" : "grafici") + ".zip");
					});
					
					$scope.downloadInProgress = false;
				};
				
				var arJson, arCsv, arPng = [];
				
				//use a worker to generate json and csv since it is a DOM-independent operation
				//currently the usage of a worker is futile since the postMessage() call that starts the worker executes only after the function exits, i.e. after the png generation has finished
				var worker = (function(){
					var blobURL = URL.createObjectURL(new Blob(['(',
					function(){	//worker body
						//make sure the following two functions are always identical to their counterparts in the main script
						//at the time being, there are only dirty solutions for passing a function from the main script to a worker 
						var generateJSON = function(data){
							return JSON.stringify(data, null, "\t");
						},
						generateCSV = function(data){
							var effectiveData = data.data;
							
							var result = "";
							var newline = "\r\n";
							
							//header
							result += "\"programmeName\",\"programmeStart\",\"programmeEnd\",\"popularity (percent)\"";
							
							//lines
							for(var i = 0, len = effectiveData.length; i < len; i++){
								var emission = effectiveData[i].programmeRecord;
								var popularity = effectiveData[i].value;
								
								result += newline;
								result += "\"" + emission.name + "\"," + emission.start + "," + emission.end + "," + popularity;
							}
							
							return result;
						};
						
						self.addEventListener('message', function(event){
							var input = event.data,
								json = [],
								csv = [];
							
							//processing
							for(var i = 0, len = input.length; i < len; i++){
								var data = input[i];
								
								json.push(generateJSON(data));
								csv.push(generateCSV(data));
							}
							
							//callback
							self.postMessage({
								"json": json,
								"csv": csv
							});
							
							//terminate
							self.close();
						}, false);
					}.toString(),
					')()'], {type:'application/javascript'})),
		
					worker = new Worker(blobURL);
		
					URL.revokeObjectURL(blobURL);
					
					return worker;
				})();
				
				worker.addEventListener('message', function(event) {
					var data = event.data;
					
					arJson = data.json;
					arCsv = data.csv;
					
					worker = null;
					
					if(arPng.length == results.length){
						//main script has finished first, archive and download results
						archiveAndDownload(results, arJson, arCsv, arPng);
					} //else the main script hasn't finished yet - it will initiate archiving and download when it finishes 
				}, false);

				//start worker
				worker.postMessage(results);
					
				//use main execution to generate charts
				for(var i = 0, len = results.length; i < len; i++){
					arPng.push(generatePNG(results[i]).data);
				}
				
				if(!worker){
					//worker has finished first, archive and download results
					archiveAndDownload(results, arJson, arCsv, arPng);
				} //else the worker hasn't finished yet - archiving and download will occur when it finishes 
			}
		};
		
		$scope.selectedResultChange = function(){
			updateChartPreview();
		};
		
		$scope.displayLargeChartPreview = function($event){
			var dialogParent = angular.element(document.body);
			
			var imgData = angular.element("div.preview-container img.chart-preview").attr("src");
			var imgTitle = getResultTitle(results[$scope.selectedResultIndex]);
			
	        $mdDialog.show({
	          parent: dialogParent,
	          targetEvent: $event,
	          template:
	            '<md-dialog aria-label="List dialog">' +
	            '  <md-dialog-content>'+
	            '    <img ng-src="{{image.data}}" style="display: block; margin: 0 auto;" ng-mousedown="mouseDown($event);" ng-mousemove="mouseMove($event);" ng-mouseup="mouseUp($event);" ng-mouseleave="mouseLeave($event);" />'+
	            '  </md-dialog-content>' +
	            '  <md-dialog-actions>' +
	            '    <span style="position: absolute; left: 15px; bottom: 15px;">{{image.title}}</span>' + 
	            '    <md-button ng-click="closeDialog()" class="md-primary">' +
	            '      Затвори' +
	            '    </md-button>' +
	            '  </md-dialog-actions>' +
	            '</md-dialog>',
	          locals: {
	            image: {
	            	 data: imgData,
	            	 title: imgTitle
	            }
	          },
	          controller: function($scope, $mdDialog, image){
		         $scope.image = image;
		         
		         $scope.closeDialog = function() {
		         	 $mdDialog.hide();
		         }
		         
		         //implement panning with left mouse button
		         
		         var dragging = false,
		             clickPosX, clickPosY,
		             oldPosX, oldPosY,
		             movePosX, movePosY;
		         
		         //start panning
		         $scope.mouseDown = function($event){
		        	 clickPosX = $event.pageX;
		        	 clickPosY = $event.pageY;
		        	 
		        	 oldPosX = $event.pageX;
		        	 oldPosY = $event.pageY;
		        	 
		        	 dragging = true;
		        	 
		        	 //prevents the browser from adding its own cursor
		        	 $event.originalEvent.preventDefault();
		        	 
		        	 //update cursor
		        	 angular.element($event.target).css('cursor', 'move');
		         };
		         
		         //if panning, move to new location
		         $scope.mouseMove = function($event){
		        	 movePosX = $event.pageX;
	        	     movePosY = $event.pageY;
	        	     
	        	     if(dragging){
	        	    	 //mixing jquery and angular is a bad practice
	        	    	 //how can one scroll an element with angular?
	        	    	 var parent = $($event.target.parentNode);
	        	    	 
	        	         parent.scrollTop(parent.scrollTop() + (oldPosY-movePosY));
	        	         parent.scrollLeft(parent.scrollLeft() + (oldPosX-movePosX));
	        	        
	        	         oldPosX = movePosX;
	        	         oldPosY = movePosY;
	        	     }
		         };
		         
		         //stop panning
		         $scope.mouseUp = function($event){
		        	 dragging = false;
	        	     
		        	 //update cursor
	        	     angular.element($event.target).css('cursor', 'default');
		         };
		         
		         //angularjs calls mouseout mouseleave
		         //if panning, stop panning
		         $scope.mouseLeave = function($event){
		        	 if(dragging){
			        	 dragging = false;
		        	     
			        	 //update cursor
		        	     angular.element($event.target).css('cursor', 'default');
		        	 }
		         };
			  }
	       });
		};
		
		$scope.executeChartGeneration = function(){
			if(!$scope.executionInProgress && !$scope.downloadInProgress){
				$scope.executionInProgress = true;
				$scope.errors = [];
				$scope.inputErrors = [];
				results = [];
				
				$scope.haveResults = false;
				$scope.resultItems.splice(0, $scope.resultItems.length);
				
				if($scope.selectedPeriodFrom.getTime() > $scope.selectedPeriodTo.getTime()){
					//swap dates
					var temp = $scope.selectedPeriodFrom;
					$scope.selectedPeriodFrom = $scope.selectedPeriodTo;
					$scope.selectedPeriodTo = temp;
				}
				
				setCookie("from", $scope.selectedPeriodFrom.getTime(), 365);
				setCookie("to", $scope.selectedPeriodTo.getTime(), 365);
				
				//array contains: startDate, month1End, month2Start, month2End, month3Start, month3End, month4Start, ..., endDate
				var chartsPeriodIntervals = getChartsPeriods($scope.selectedPeriodFrom, $scope.selectedPeriodTo);
				
				//process request
				var channelsToProcess = [];
				if($scope.selectedChannel == $scope.channels[0]){
					for(var i = 1, len = $scope.channels.length; i < len; i++){
						channelsToProcess.push($scope.channels[i]);
					}
				}else{
					channelsToProcess.push($scope.selectedChannel);
				}
				
				if($scope.selectedUser == $scope.users[0]){
					if($scope.selectedTotalUsers == "" || isNaN($scope.selectedTotalUsers)){
						$scope.inputErrors.push("Внесете го бројот на корисници во системот.");
						$scope.executionInProgress = false;
						
						return;
					}
					
					var totalUsers = $scope.selectedTotalUsers;
					setCookie("totalUsers", totalUsers, 365);
					
					//aggregate
					totalCharts = (chartsPeriodIntervals.length / 2) * channelsToProcess.length;  
					for(var d = 0, lend = chartsPeriodIntervals.length; d < lend; d += 2){
						for(var c = 0, lenc = channelsToProcess.length; c < lenc; c++){
							(function(channel, from, to, totalUsers){
								ChartService.getChannelChart(channel, from, to, totalUsers)
								.success(function(data){
									results.push(data);
									
									if(results.length == totalCharts){
										$scope.executionInProgress = false;
										processResults();
									}
								}).error(function(data, status){
									$scope.errors.push("Статус на одговорот: " + status + " (" + data.error + ")");
									console.log(data.error);
									
									results.push(null);
									
									if(results.length == totalCharts){
										$scope.executionInProgress = false;
										processResults();
									}
								});
							})(channelsToProcess[c], chartsPeriodIntervals[d], chartsPeriodIntervals[d + 1], totalUsers);
						}
					}
				}else{
					//single user
					var userid = $scope.selectedUser.userId;
					
					totalCharts = (chartsPeriodIntervals.length / 2) * channelsToProcess.length;
					for(var d = 0, lend = chartsPeriodIntervals.length; d < lend; d += 2){
						for(var c = 0, lenc = channelsToProcess.length; c < lenc; c++){
							(function(userid, channel, from, to){
								ChartService.getChannelChartForSingleUser(userid, channel, from, to)
								.success(function(data){
									results.push(data);
									
									if(results.length == totalCharts){
										$scope.executionInProgress = false;
										processResults();
									}
								}).error(function(data, status){
									$scope.errors.push("Статус на одговорот: " + status + " (" + data.error + ")");
									console.log(data.error);
									
									results.push(null);
									
									if(results.length == totalCharts){
										$scope.executionInProgress = false;
										processResults();
									}
								});
							})(userid, channelsToProcess[c], chartsPeriodIntervals[d], chartsPeriodIntervals[d + 1]);
						}
					}
				}
			}
		};
		
		$scope.userFilterChange = function(){
			var filterStr = $scope.userFilter;
			if(filterStr && filterStr != "" && filterStr.trim() != ""){
				filterStr = filterStr.toLowerCase();
				
				$scope.filteredUsers.splice(0, $scope.filteredUsers.length);
				
				if(!filterStr.includes(" ")){
					//filter by first name OR last name OR username
					var arr = $scope.users,
					    len = arr.length;
					for(var i = 1; i < len; i++){	//discarding 'all users' option when using filter
						var el = arr[i];
						if(el.firstName.toLowerCase().startsWith(filterStr) || el.lastName.toLowerCase().startsWith(filterStr) || el.username.toLowerCase().startsWith(filterStr)){
							$scope.filteredUsers.push(el);
						}
					}
				}else{
					//filter by (first name, last name)
					var sp = filterStr.split(" "),
						fnPart = sp.splice(0, 1)[0],
						lnPart = sp.join(" ");
					
					var arr = $scope.users,
					    len = arr.length;
					for(var i = 1; i < len; i++){	//discarding 'all users' option when using filter
						var el = arr[i];
						if(el.firstName.toLowerCase().startsWith(fnPart) && el.lastName.toLowerCase().startsWith(lnPart)){
							$scope.filteredUsers.push(el);
						}
					}
				}
			}else{
				//no filter
				var holder = $scope.users.slice(),
					len = holder.length;;
				for(var i = 0; i < len; i++){
					$scope.filteredUsers.push(holder[i]);
				}
			}
			
			$scope.selectedUser = $scope.filteredUsers.length > 0 ? $scope.filteredUsers[0] : null;
		};
		
		$rootScope.$on('staticDataFetch', function(){
			var fetchedChannels = ChartService.getChannels();
			if(fetchedChannels){
				$scope.channels.splice(0, $scope.channels.length);
				$scope.channels = fetchedChannels;
				$scope.channels.splice(0, 0, "сите канали");
			}
			
			var fetchedUsers = ChartService.getUsers();
			if(fetchedUsers){
				$scope.users.splice(0, $scope.users.length);
				$scope.users = fetchedUsers; 
				$scope.users.splice(0, 0, "сите корисници");
				
				$scope.filteredUsers.splice(0, $scope.filteredUsers.length);
				for(var i = 0, len = $scope.users.length; i < len; i++){
					$scope.filteredUsers.push($scope.users[i]);
				}
			}
        });
		
		ChartService.fetchStaticData();
	}]);