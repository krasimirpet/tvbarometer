'use strict';

TVBarometerApp.config([ '$stateProvider', '$urlRouterProvider',
  function($stateProvider, $urlRouterProvider) {
	
	$stateProvider

    .state('chart', {
	  url: "/",
      templateUrl: "app/views/chart.html",
      controller: "ChartCtrl",
      data: {
		  css: ["app/styles/chart.css"]
	  }
    })
    
    .state('usage', {
      url: "/usage",
      templateUrl: "app/views/usage.html",
	  controller: "UsageCtrl",
	  data: {
		  css: ["app/styles/usage.css"]
	  }
    });
	
	//if another url is in place, go to index
	$urlRouterProvider.otherwise('/');

}]);