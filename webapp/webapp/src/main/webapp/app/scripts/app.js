'use strict';

var TVBarometerApp = angular.module('tvbarometerApp',  ['ngResource', 'ngRoute', 'ngMaterial', 'ui.router', 'uiRouterStyles']);

//date format configuration for md-datepicker
TVBarometerApp.config(function($mdDateLocaleProvider) {
	$mdDateLocaleProvider.formatDate = function(date) {
		var day = date.getDate();
		var month = date.getMonth() + 1;
		var year = date.getFullYear();
	   
		return day + "." + month + "." + year;
	};

	$mdDateLocaleProvider.parseDate = function(dateString) {
		var dateParts = dateString.split(".");
		if(dateParts && dateParts.length == 3){
			var dayInMonth = dateParts[0];
			var monthIndex = dateParts[1] - 1;
			var year = dateParts[2];
			
			return new Date(year, monthIndex, dayInMonth);
		}else{
			return new Date(NaN);
		}
	};
});