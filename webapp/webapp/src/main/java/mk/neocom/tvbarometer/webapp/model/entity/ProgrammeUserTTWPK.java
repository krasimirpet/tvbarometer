package mk.neocom.tvbarometer.webapp.model.entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class ProgrammeUserTTWPK implements Serializable {
	
	private User user;
	private String channelName;
	private Timestamp programmeStart;
	
	public ProgrammeUserTTWPK(){}
	
	public ProgrammeUserTTWPK(User user, String channelName, Timestamp programmeStart){
		this.user = user;
		this.channelName = channelName;
		this.programmeStart = programmeStart;
	}
	
	public User getUser(){
		return user;
	}
	
	public void setUser(User user){
		this.user = user;
	}
	
	public String getChannelName(){
		return channelName;
	}
	
	public void setChannelName(String channelName){
		this.channelName = channelName;
	}
	
	public Timestamp getProgrammeStart(){
		return programmeStart;
	}
	
	public void setProgrammeStart(Timestamp programmeStart){
		this.programmeStart = programmeStart;
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof ProgrammeUserTTWPK){
			ProgrammeUserTTWPK other = (ProgrammeUserTTWPK)o;
			return this.user.equals(other.getUser()) && this.channelName.equals(other.getChannelName()) && this.programmeStart.equals(other.getProgrammeStart());
		}else{
			return false;
		}
	}
	
	@Override
	public int hashCode(){
		return this.user.hashCode() ^ this.channelName.hashCode() ^ this.programmeStart.hashCode();
	}
}