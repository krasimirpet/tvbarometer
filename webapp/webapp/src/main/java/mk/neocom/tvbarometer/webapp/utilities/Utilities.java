package mk.neocom.tvbarometer.webapp.utilities;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Utilities {
	
	public static java.sql.Timestamp utilDateToSQLTimestamp(Date date){
		return new java.sql.Timestamp(date.getTime());
	}
	
	public static Date SQLTimestampToUtilDate(java.sql.Timestamp timestamp){
		return new Date(timestamp.getTime());
	}
	
	public static Date parseDate(String date) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
		return sdf.parse(date);
	}
	
	public static String formatDate(Date date){
		SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy", Locale.ENGLISH);
		return sdf.format(date);
	}
}