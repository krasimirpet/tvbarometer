package mk.neocom.tvbarometer.webapp.model.entity;

import java.io.Serializable;
import java.sql.Timestamp;

public class EmissionPK implements Serializable {
	
	private String channelName;
	private Timestamp programmeStart;
	
	public EmissionPK(){}
	
	public EmissionPK(String channelName, Timestamp programmeStart){
		this.channelName = channelName;
		this.programmeStart = programmeStart;
	}
	
	public String getChannelName(){
		return channelName;
	}
	
	public void setChannelName(String channelName){
		this.channelName = channelName;
	}
	
	public Timestamp getProgrammeStart(){
		return programmeStart;
	}
	
	public void setProgrammeStart(Timestamp programmeStart){
		this.programmeStart = programmeStart;
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof EmissionPK){
			EmissionPK other = (EmissionPK)o;
			return this.channelName.equals(other.getChannelName()) && this.programmeStart.equals(other.getProgrammeStart());
		}else{
			return false;
		}
	}
	
	@Override
	public int hashCode(){
		return this.channelName.hashCode() ^ this.programmeStart.hashCode();
	}
}