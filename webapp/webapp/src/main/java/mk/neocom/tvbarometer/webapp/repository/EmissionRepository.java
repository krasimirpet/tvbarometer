package mk.neocom.tvbarometer.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mk.neocom.tvbarometer.webapp.model.entity.Emission;
import mk.neocom.tvbarometer.webapp.model.entity.EmissionPK;

public interface EmissionRepository extends JpaRepository<Emission, EmissionPK>, EmissionRepositoryAux {
	
}