package mk.neocom.tvbarometer.webapp.model.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.Table;

@Entity
@Table(name = "Emissions")
@IdClass(EmissionPK.class)
public class Emission {
	
	@Id
	@Column(name="channelName", nullable=false)
	private String channelName;
	
	@Id
	@Column(name="programmeStart", nullable=false)
	private Timestamp programmeStart;
	
	@Column(name="programmeEnd", nullable=false)
	private Timestamp programmeEnd;
	
	@Column(name="programmeName", nullable=false)
	private String programmeName;
	
	public Emission(){}
	
	public String getChannelName(){
		return channelName;
	}
	
	public void setChannelName(String channelName){
		this.channelName = channelName;
	}
	
	public Timestamp getProgrammeStart(){
		return programmeStart;
	}
	
	public void setProgrammeStart(Timestamp programmeStart){
		this.programmeStart = programmeStart;
	}
	
	public Timestamp getProgrammeEnd(){
		return programmeEnd;
	}
	
	public void setProgrammeEnd(Timestamp programmeEnd){
		this.programmeEnd = programmeEnd;
	}
	
	public String getProgrammeName(){
		return programmeName;
	}
	
	public void setProgrammeName(String programmeName){
		this.programmeName = programmeName;
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof Emission){
			Emission other = (Emission)o;
			return this.channelName.equals(other.getChannelName()) && this.programmeStart.equals(other.getProgrammeStart());
		}else{
			return false;
		}
	}
	
	@Override
	public int hashCode(){
		return this.channelName.hashCode() ^ this.programmeStart.hashCode();
	}
}