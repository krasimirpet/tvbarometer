package mk.neocom.tvbarometer.webapp.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import mk.neocom.tvbarometer.webapp.model.entity.Emission;
import mk.neocom.tvbarometer.webapp.model.entity.ProgrammeUserTTW;
import mk.neocom.tvbarometer.webapp.model.json.ProgrammeRecord;
import mk.neocom.tvbarometer.webapp.model.json.User;
import mk.neocom.tvbarometer.webapp.repository.EmissionRepository;
import mk.neocom.tvbarometer.webapp.repository.ProgrammeUserTTWRepository;
import mk.neocom.tvbarometer.webapp.repository.UserRepository;
import mk.neocom.tvbarometer.webapp.service.ChartService;
import mk.neocom.tvbarometer.webapp.utilities.Utilities;

@Service
public class ChartServiceImpl implements ChartService {
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ProgrammeUserTTWRepository programmeUserTTWRepository;
	
	@Autowired
	private EmissionRepository emissionRepository;

	public Map<ProgrammeRecord, Float> getChannelChartData(String channel, Date from, Date to, int totalUsers) {
		Map<ProgrammeRecord, Float> results = new TreeMap<ProgrammeRecord, Float>();
		
		to = this.correctIntervalEnd(to);
		
		List<ProgrammeUserTTW> puttws = programmeUserTTWRepository.calculateTTW(channel, from, to);
		List<Emission> emissions = emissionRepository.getEmissionsForChannelInTimePeriod(channel, from, to);
		if(puttws == null || emissions == null){
			return null;
		}
		
		for(ProgrammeUserTTW puttw : puttws){
			Date programmeStart = Utilities.SQLTimestampToUtilDate(puttw.getProgrammeStart());
			Date programmeEnd = Utilities.SQLTimestampToUtilDate(puttw.getProgrammeEnd());
			
			ProgrammeRecord pr = new ProgrammeRecord(programmeStart, programmeEnd, puttw.getProgrammeName());
			
			results.put(pr, puttw.getTimeWatched() / (this.getProgrammeDurationInMiutes(programmeStart, programmeEnd) * totalUsers));
		}
		
		for(Emission emission : emissions){
			ProgrammeRecord pr = new ProgrammeRecord(emission.getProgrammeStart(), emission.getProgrammeEnd(), emission.getProgrammeName());
			
			if(!results.containsKey(pr)){
				results.put(pr, 0f);
			}
		}
		
		return results;
	}

	public Map<ProgrammeRecord, Float> getChannelChartDataForUser(String userid, String channel, Date from, Date to) {
		Map<ProgrammeRecord, Float> results = new TreeMap<ProgrammeRecord, Float>();
		
		to = this.correctIntervalEnd(to);
		
		List<ProgrammeUserTTW> puttws = programmeUserTTWRepository.getTTWForUser(userid, channel, from, to);
		List<Emission> emissions = emissionRepository.getEmissionsForChannelInTimePeriod(channel, from, to);
		if(puttws == null || emissions == null){
			return null;
		}
		
		for(ProgrammeUserTTW puttw : puttws){
			Date programmeStart = Utilities.SQLTimestampToUtilDate(puttw.getProgrammeStart());
			Date programmeEnd = Utilities.SQLTimestampToUtilDate(puttw.getProgrammeEnd());
			
			ProgrammeRecord pr = new ProgrammeRecord(programmeStart, programmeEnd, puttw.getProgrammeName());
			
			results.put(pr, puttw.getTimeWatched() / this.getProgrammeDurationInMiutes(programmeStart, programmeEnd));
		}
		
		for(Emission emission : emissions){
			ProgrammeRecord pr = new ProgrammeRecord(emission.getProgrammeStart(), emission.getProgrammeEnd(), emission.getProgrammeName());
			
			if(!results.containsKey(pr)){
				results.put(pr, 0f);
			}
		}
		
		return results;
	}
	
	public List<String> getAllChannels() {
		return programmeUserTTWRepository.getChannels();
	}
	
	public List<User> getAllUsers() {
		List<Sort.Order> sortOrders = new LinkedList<Sort.Order>();
		sortOrders.add(new Sort.Order(Sort.Direction.ASC, "lastName"));
		sortOrders.add(new Sort.Order(Sort.Direction.ASC, "firstName"));
		sortOrders.add(new Sort.Order(Sort.Direction.ASC, "username"));
		
		List<mk.neocom.tvbarometer.webapp.model.entity.User> users = userRepository.findAll(new Sort(sortOrders));
		
		List<User> results = new LinkedList<User>();
		for(mk.neocom.tvbarometer.webapp.model.entity.User user : users){
			results.add(new User(user.getUserId(), user.getFirstName(), user.getLastName(), user.getUsername()));
		}
		
		return results;
	}
	
	public User getUser(String userid) {
		mk.neocom.tvbarometer.webapp.model.entity.User user = userRepository.findOne(userid);
		
		if(user != null){
			return new User(user.getUserId(), user.getFirstName(), user.getLastName(), user.getUsername());
		}else{
			return null;
		}
	}
	
	private float getProgrammeDurationInMiutes(Date programmeStart, Date programmeEnd){
		return (programmeEnd.getTime() - programmeStart.getTime()) / (60 * 1000.0f);
	}
	
	private Date correctIntervalEnd(Date end){
		//shift interval end to midnight the next day
		
		Calendar calendar = Calendar.getInstance();
		calendar.setTime(end);
		
		calendar.add(Calendar.DATE, 1);
		
		calendar.set(Calendar.HOUR, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
		calendar.set(Calendar.MILLISECOND, 0);
		
		return calendar.getTime();
	}
}