package mk.neocom.tvbarometer.webapp.model.json;

import java.util.List;

public class ChartData {
	
	private String channel;
	
	private String from;
	private String to;
	
	private List<ChartDataKeyValuePair> data;
	
	public ChartData(){}
	
	public ChartData(String channel, String from, String to, List<ChartDataKeyValuePair> data){
		this.channel = channel;
		this.from = from;
		this.to = to;
		this.data = data;
	}
	
	public String getChannel(){
		return channel;
	}
	
	public void setChannel(String channel){
		this.channel = channel;
	}
	
	public String getFrom(){
		return from;
	}
	
	public void setFrom(String from){
		this.from = from;
	}
	
	public String getTo(){
		return to;
	}
	
	public void setTo(String to){
		this.to = to;
	}
	
	public List<ChartDataKeyValuePair> getData(){
		return data;
	}
	
	public void setData(List<ChartDataKeyValuePair> data){
		this.data = data;
	}
	
	public static class ChartDataKeyValuePair {
		private ProgrammeRecord programmeRecord;
		private Float value;
		
		public ChartDataKeyValuePair(){}
		
		public ChartDataKeyValuePair(ProgrammeRecord programmeRecord, Float value){
			this.programmeRecord = programmeRecord;
			this.value = value;
		}
		
		public ProgrammeRecord getProgrammeRecord(){
			return programmeRecord;
		}
		
		public void setProgrammeRecord(ProgrammeRecord programmeRecord){
			this.programmeRecord = programmeRecord;
		}
		
		public Float getValue(){
			return value;
		}
		
		public void setValue(Float value){
			this.value = value;
		}
	}
}