package mk.neocom.tvbarometer.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mk.neocom.tvbarometer.webapp.model.entity.ProgrammeUserTTW;
import mk.neocom.tvbarometer.webapp.model.entity.ProgrammeUserTTWPK;

public interface ProgrammeUserTTWRepository extends JpaRepository<ProgrammeUserTTW, ProgrammeUserTTWPK>, ProgrammeUserTTWRepositoryAux {

}