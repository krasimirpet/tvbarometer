package mk.neocom.tvbarometer.webapp.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import mk.neocom.tvbarometer.webapp.model.entity.User;

public interface UserRepository extends JpaRepository<User, String>{
	
}