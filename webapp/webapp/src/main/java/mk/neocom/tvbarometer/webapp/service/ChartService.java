package mk.neocom.tvbarometer.webapp.service;

import java.util.Date;
import java.util.List;
import java.util.Map;

import mk.neocom.tvbarometer.webapp.model.json.ProgrammeRecord;
import mk.neocom.tvbarometer.webapp.model.json.User;

public interface ChartService {
	
	Map<ProgrammeRecord, Float> getChannelChartData(String channel, Date from, Date to, int totalUsers);
	
	Map<ProgrammeRecord, Float> getChannelChartDataForUser(String userid, String channel, Date from, Date to);
	
	List<String> getAllChannels();
	
	List<User> getAllUsers();
	
	User getUser(String userid);
}