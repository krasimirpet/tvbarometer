package mk.neocom.tvbarometer.webapp.model.json;

import java.util.List;

public class UserChartData extends ChartData {
	
	private User user;
	
	public UserChartData(){
		super();
	}
	
	public UserChartData(String channel, User user, String from, String to, List<ChartDataKeyValuePair> data){
		super(channel, from, to, data);
		
		this.user = user;
	}
	
	public User getUser(){
		return user;
	}
	
	public void setUser(User user){
		this.user = user;
	}
}