package mk.neocom.tvbarometer.webapp.repository;

import java.util.Date;
import java.util.List;

import mk.neocom.tvbarometer.webapp.model.entity.Emission;

public interface EmissionRepositoryAux {
	
	List<Emission> getEmissionsForChannelInTimePeriod(String channel, Date from, Date to);
}