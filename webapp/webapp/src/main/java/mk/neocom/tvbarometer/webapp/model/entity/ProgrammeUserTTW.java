package mk.neocom.tvbarometer.webapp.model.entity;

import java.sql.Timestamp;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ProgrammesUsersTTW")
@IdClass(ProgrammeUserTTWPK.class)
public class ProgrammeUserTTW {
	
	@ManyToOne
	@Id
	@JoinColumn(name="userId")
	private User user;
	
	@Id
	@Column(name="channelName", nullable=false)
	private String channelName;
	
	@Id
	@Column(name="programmeStart", nullable=false)
	private Timestamp programmeStart;
	
	@Column(name="programmeEnd", nullable=false)
	private Timestamp programmeEnd;
	
	@Column(name="programmeName", nullable=false)
	private String programmeName;
	
	@Column(name="timeWatched", nullable=false)
	private float timeWatched;

	public ProgrammeUserTTW(){}
	
	public User getUser(){
		return user;
	}
	
	public void setUser(User user){
		this.user = user;
	}
	
	public String getChannelName(){
		return channelName;
	}
	
	public void setChannelName(String channelName){
		this.channelName = channelName;
	}
	
	public Timestamp getProgrammeStart(){
		return programmeStart;
	}
	
	public void setProgrammeStart(Timestamp programmeStart){
		this.programmeStart = programmeStart;
	}
	
	public Timestamp getProgrammeEnd(){
		return programmeEnd;
	}
	
	public void setProgrammeEnd(Timestamp programmeEnd){
		this.programmeEnd = programmeEnd;
	}
	
	public String getProgrammeName(){
		return programmeName;
	}
	
	public void setProgrammeName(String programmeName){
		this.programmeName = programmeName;
	}
	
	public float getTimeWatched(){
		return timeWatched;
	}
	
	public void setTimeWatched(float timeWatched){
		this.timeWatched = timeWatched;
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof ProgrammeUserTTW){
			ProgrammeUserTTW other = (ProgrammeUserTTW)o;
			return this.user.equals(other.getUser()) && this.channelName.equals(other.getChannelName()) && this.programmeStart.equals(other.getProgrammeStart());
		}else{
			return false;
		}
	}
	
	@Override
	public int hashCode(){
		return this.user.hashCode() ^ this.channelName.hashCode() ^ this.programmeStart.hashCode();
	}
}