package mk.neocom.tvbarometer.webapp.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mk.neocom.tvbarometer.webapp.model.entity.ProgrammeUserTTW;
import mk.neocom.tvbarometer.webapp.repository.ProgrammeUserTTWRepositoryAux;
import mk.neocom.tvbarometer.webapp.utilities.Utilities;

@Repository
public class ProgrammeUserTTWRepositoryImpl implements ProgrammeUserTTWRepositoryAux {
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Transactional
	public List<ProgrammeUserTTW> calculateTTW(String channel, Date from, Date to) {
		try{
			final String finalChannel = channel;
			final Timestamp finalFrom = Utilities.utilDateToSQLTimestamp(from);
			final Timestamp finalTo = Utilities.utilDateToSQLTimestamp(to);
			
			Session session = entityManager.unwrap(Session.class);
			return session.doReturningWork(new ReturningWork<List<ProgrammeUserTTW>>(){
			    public List<ProgrammeUserTTW> execute(Connection connection) {
			    	try{
				    	//calculate TTW
			    		Calendar timezoneAdjustedCalendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Skopje"));
			    		Calendar gmtCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			    		
				        PreparedStatement calculateTTW = connection.prepareStatement("SELECT ProgrammesUsersTTW.programmeStart AS ProgrammeStart, ProgrammesUsersTTW.programmeEnd AS ProgrammeEnd, ProgrammesUsersTTW.programmeName AS ProgrammeName, SUM(ProgrammesUsersTTW.timeWatched) AS TotalTimeWatched FROM ProgrammesUsersTTW WHERE ProgrammesUsersTTW.channelName = ? AND ProgrammesUsersTTW.programmeStart >= ? AND ProgrammesUsersTTW.programmeEnd < ? GROUP BY (ProgrammesUsersTTW.programmeStart, ProgrammesUsersTTW.programmeEnd, ProgrammesUsersTTW.programmeName) ORDER BY ProgrammesUsersTTW.programmeStart");
				        calculateTTW.setString(1, finalChannel);
				        calculateTTW.setTimestamp(2, finalFrom, timezoneAdjustedCalendar);
				        calculateTTW.setTimestamp(3, finalTo, timezoneAdjustedCalendar);
				        
				        ResultSet results = calculateTTW.executeQuery();
				        
				        List<ProgrammeUserTTW> resultList = new LinkedList<ProgrammeUserTTW>();
				        while(results.next()){
				        	ProgrammeUserTTW puttw = new ProgrammeUserTTW();
				        	
				        	puttw.setProgrammeStart(results.getTimestamp("ProgrammeStart", gmtCalendar));
				        	puttw.setProgrammeEnd(results.getTimestamp("ProgrammeEnd", gmtCalendar));
				        	puttw.setProgrammeName(results.getString("ProgrammeName"));
				        	puttw.setTimeWatched(results.getFloat("TotalTimeWatched"));
				        	
				        	resultList.add(puttw);
				        }
				        
				        results.close();
				        calculateTTW.close();
				        
				        return resultList;
			    	}catch(SQLException ex){
			    		return null;
			    	}
			    }
			});
		}catch(HibernateException ex){
			return null;
		}
	}
	
	@Transactional
	public List<ProgrammeUserTTW> getTTWForUser(String userid, String channel, Date from, Date to) {
		try{
			final String finalUser = userid;
			final String finalChannel = channel;
			final Timestamp finalFrom = Utilities.utilDateToSQLTimestamp(from);
			final Timestamp finalTo = Utilities.utilDateToSQLTimestamp(to);
			
			Session session = entityManager.unwrap(Session.class);
			return session.doReturningWork(new ReturningWork<List<ProgrammeUserTTW>>(){
			    public List<ProgrammeUserTTW> execute(Connection connection) throws SQLException {
			    	try{
			    		Calendar timezoneAdjustedCalendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Skopje"));
			    		Calendar gmtCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));	//by default the application operates with Europe/Skopje timezones, therefore do not timezone shift the date on reading
			    		
				        PreparedStatement getTTW = connection.prepareStatement("SELECT ProgrammesUsersTTW.programmeStart AS ProgrammeStart, ProgrammesUsersTTW.programmeEnd AS ProgrammeEnd, ProgrammesUsersTTW.programmeName AS ProgrammeName, ProgrammesUsersTTW.timeWatched AS TimeWatched FROM ProgrammesUsersTTW WHERE ProgrammesUsersTTW.userId = ? AND ProgrammesUsersTTW.channelName = ? AND ProgrammesUsersTTW.programmeStart >= ? AND ProgrammesUsersTTW.programmeEnd < ? ORDER BY ProgrammesUsersTTW.programmeStart");
				        getTTW.setString(1, finalUser);
				        getTTW.setString(2, finalChannel);
				        getTTW.setTimestamp(3, finalFrom, timezoneAdjustedCalendar);
				        getTTW.setTimestamp(4, finalTo, timezoneAdjustedCalendar);
				        
				        ResultSet results = getTTW.executeQuery();
				        
				        List<ProgrammeUserTTW> resultList = new LinkedList<ProgrammeUserTTW>();
				        while(results.next()){
				        	ProgrammeUserTTW puttw = new ProgrammeUserTTW();
				        	
				        	puttw.setProgrammeStart(results.getTimestamp("ProgrammeStart", gmtCalendar));
				        	puttw.setProgrammeEnd(results.getTimestamp("ProgrammeEnd", gmtCalendar));
				        	puttw.setProgrammeName(results.getString("ProgrammeName"));
				        	puttw.setTimeWatched(results.getFloat("TimeWatched"));
				        	
				        	resultList.add(puttw);
				        }
				        
				        results.close();
				        getTTW.close();
				        
				        return resultList;
			    	}catch(SQLException ex){
			    		return null;
			    	}
			    }
			});
		}catch(HibernateException ex){
			return null;
		}
	}

	@Transactional
	public List<String> getChannels(){
		try{
			Session session = entityManager.unwrap(Session.class);
			return session.doReturningWork(new ReturningWork<List<String>>(){
			    public List<String> execute(Connection connection) throws SQLException {
			    	try{
				        Statement getChannels = connection.createStatement();
				        ResultSet results = getChannels.executeQuery("SELECT DISTINCT channelName AS ChannelName FROM ProgrammesUsersTTW UNION SELECT DISTINCT channelName AS ChannelName FROM Emissions ORDER BY ChannelName ASC");
				        
				        List<String> resultList = new LinkedList<String>();
				        while(results.next()){
				        	resultList.add(results.getString("ChannelName"));
				        }
				        
				        results.close();
				        getChannels.close();
				        
				        return resultList;
			    	}catch(SQLException ex){
			    		return null;
			    	}
			    }
			});
		}catch(HibernateException ex){
			return null;
		}
	}
}