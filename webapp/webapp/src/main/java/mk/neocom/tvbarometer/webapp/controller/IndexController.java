package mk.neocom.tvbarometer.webapp.controller;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import mk.neocom.tvbarometer.webapp.model.json.ChartData;
import mk.neocom.tvbarometer.webapp.model.json.ChartData.ChartDataKeyValuePair;
import mk.neocom.tvbarometer.webapp.model.json.ProgrammeRecord;
import mk.neocom.tvbarometer.webapp.model.json.User;
import mk.neocom.tvbarometer.webapp.model.json.UserChartData;
import mk.neocom.tvbarometer.webapp.service.ChartService;
import mk.neocom.tvbarometer.webapp.utilities.Utilities;

@Controller
public class IndexController {
	
	@Autowired
	private ChartService charter;
	
	@RequestMapping(value={"", "/"}, method=RequestMethod.GET)
	public ModelAndView index() {
		return new ModelAndView("index");
	}
	
	@RequestMapping(value={"/chart/total"}, method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> getChannelChart(@RequestParam String channel, @RequestParam String from, @RequestParam String to, @RequestParam Integer totalUsers){
		Date dateFrom = null;
		Date dateTo = null;
		
		try{
			dateFrom = Utilities.parseDate(from);
			dateTo = Utilities.parseDate(to);
		}catch(ParseException e){
			return new ResponseEntity<String>("{\"error\":\"can not read date parameters - format is dd.MM.yyyy\"}", HttpStatus.BAD_REQUEST);
		}
		
		if(totalUsers == null || totalUsers <= 0){
			return new ResponseEntity<String>("{\"error\":\"totalUsers value is illegal - accepted values are numbers greater than zero\"}", HttpStatus.BAD_REQUEST);
		}
		
		Map<ProgrammeRecord, Float> data = charter.getChannelChartData(channel, dateFrom, dateTo, totalUsers);
		if(data == null){
			return new ResponseEntity<String>("{\"error\":\"internal server error - it's not your fault and there's nothing you can do about it\"}", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		List<ChartDataKeyValuePair> dataList = new ArrayList<ChartDataKeyValuePair>(data.keySet().size());
		for(ProgrammeRecord programmeRecord : data.keySet()){
			Float value = data.get(programmeRecord);
			
			dataList.add(new ChartData.ChartDataKeyValuePair(programmeRecord, value));
		}
		
		//client sets the title
		return new ResponseEntity<ChartData>(new ChartData(channel, from, to, dataList), HttpStatus.OK);
	}
	
	@RequestMapping(value={"/chart/user"}, method=RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public ResponseEntity<?> getUserChart(@RequestParam String userid, @RequestParam String channel, @RequestParam String from, @RequestParam String to){
		Date dateFrom = null;
		Date dateTo = null;
		
		try{
			dateFrom = Utilities.parseDate(from);
			dateTo = Utilities.parseDate(to);
		}catch(ParseException e){
			return new ResponseEntity<String>("{\"error\":\"can not read date parameters - format is dd.MM.yyyy\"}", HttpStatus.BAD_REQUEST);
		}
		
		User user = charter.getUser(userid);
		if(user == null){
			return new ResponseEntity<String>("{\"error\":\"user does not exist\"}", HttpStatus.BAD_REQUEST);
		}
		
		Map<ProgrammeRecord, Float> data = charter.getChannelChartDataForUser(userid, channel, dateFrom, dateTo);
		if(data == null){
			return new ResponseEntity<String>("{\"error\":\"internal server error - it's not your fault and there's nothing you can do about it\"}", HttpStatus.INTERNAL_SERVER_ERROR);
		}
		
		List<ChartDataKeyValuePair> dataList = new ArrayList<ChartDataKeyValuePair>(data.keySet().size());
		for(ProgrammeRecord programmeRecord : data.keySet()){
			Float value = data.get(programmeRecord);
			
			dataList.add(new ChartData.ChartDataKeyValuePair(programmeRecord, value));
		}
		
		//client sets the title
		return new ResponseEntity<UserChartData>(new UserChartData(channel, user, from, to, dataList), HttpStatus.OK);
	}
	
	@RequestMapping(value={"/channels"}, method=RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<String> getChannels(){
		return charter.getAllChannels(); 
	}
	
	@RequestMapping(value={"/users"}, method=RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public List<User> getUsers(){
		return charter.getAllUsers();
	}
}