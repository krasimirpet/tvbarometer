package mk.neocom.tvbarometer.webapp.repository.impl;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.TimeZone;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.jdbc.ReturningWork;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import mk.neocom.tvbarometer.webapp.model.entity.Emission;
import mk.neocom.tvbarometer.webapp.repository.EmissionRepositoryAux;
import mk.neocom.tvbarometer.webapp.utilities.Utilities;

@Repository
public class EmissionRepositoryImpl implements EmissionRepositoryAux {
	
	@PersistenceContext
	private EntityManager entityManager; 
	
	@Transactional
	public List<Emission> getEmissionsForChannelInTimePeriod(String channel, Date from, Date to){
		try{
			final String finalChannel = channel;
			final Timestamp finalFrom = Utilities.utilDateToSQLTimestamp(from);
			final Timestamp finalTo = Utilities.utilDateToSQLTimestamp(to);
			
			Session session = entityManager.unwrap(Session.class);
			return session.doReturningWork(new ReturningWork<List<Emission>>(){
			    public List<Emission> execute(Connection connection) {
			    	try{
			    		Calendar timezoneAdjustedCalendar = Calendar.getInstance(TimeZone.getTimeZone("Europe/Skopje"));
			    		Calendar gmtCalendar = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
			    		
				        PreparedStatement getEmissions = connection.prepareStatement("SELECT Emissions.programmeStart AS ProgrammeStart, Emissions.programmeEnd AS ProgrammeEnd, Emissions.programmeName AS ProgrammeName FROM Emissions WHERE Emissions.channelName = ? AND Emissions.programmeStart >= ? AND Emissions.programmeEnd <= ? ORDER BY Emissions.programmeStart ASC");
				        getEmissions.setString(1, finalChannel);
				        getEmissions.setTimestamp(2, finalFrom, timezoneAdjustedCalendar);
				        getEmissions.setTimestamp(3, finalTo, timezoneAdjustedCalendar);
				        
				        ResultSet results = getEmissions.executeQuery();
				        
				        List<Emission> resultList = new LinkedList<Emission>();
				        while(results.next()){
				        	Emission emission = new Emission();
				        	
				        	emission.setChannelName(finalChannel);
				        	emission.setProgrammeStart(results.getTimestamp("ProgrammeStart", gmtCalendar));
				        	emission.setProgrammeEnd(results.getTimestamp("ProgrammeEnd", gmtCalendar));
				        	emission.setProgrammeName(results.getString("ProgrammeName"));
				        	
				        	resultList.add(emission);
				        }
				        
				        results.close();
				        getEmissions.close();
				        
				        return resultList;
			    	}catch(SQLException ex){
			    		return null;
			    	}
			    }
			});
		}catch(HibernateException ex){
			return null;
		}
	}
}