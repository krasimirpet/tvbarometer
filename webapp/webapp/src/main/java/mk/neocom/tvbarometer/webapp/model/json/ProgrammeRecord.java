package mk.neocom.tvbarometer.webapp.model.json;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class ProgrammeRecord implements Comparable<ProgrammeRecord>, Serializable {
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd.MM.yyyy HH:mm:ss")
	private Date start;
	
	@JsonFormat(shape=JsonFormat.Shape.STRING, pattern="dd.MM.yyyy HH:mm:ss")
	private Date end;
	
	private String name;
	
	public ProgrammeRecord(){}
	
	public ProgrammeRecord(Date start, Date end, String name){
		this.start = start;
		this.end = end;
		this.name = name;
	}
	
	public Date getStart(){
		return start;
	}
	
	public void setStart(Date start){
		this.start = start;
	}
	
	public Date getEnd(){
		return end;
	}
	
	public void setEnd(Date end){
		this.end = end;
	}
	
	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public int compareTo(ProgrammeRecord arg) {
		return this.start.compareTo(arg.getStart());
	}
}