package mk.neocom.tvbarometer.webapp.model.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "Users")
public class User {
	
	@Id
	@Column(name="userId", nullable=false)
	private String userId;
	
	@Column(name="accountId", nullable=false)
	private String accountId;
	
	@Column(name="refNumber", nullable=false)
	private String refNumber;
	
	@Column(name="firstName", nullable=false)
	private String firstName;
	
	@Column(name="lastName", nullable=false)
	private String lastName;
	
	@Column(name="username", nullable=false)
	private String username;
	
	public User(){}
	
	public String getUserId(){
		return userId;
	}
	
	public void setUserId(String userId){
		this.userId = userId;
	}
	
	public String getAccountId(){
		return accountId;
	}
	
	public void setAccountId(String accountId){
		this.accountId = accountId;
	}
	
	public String getRefNumber(){
		return refNumber;
	}
	
	public void setRefNumber(String refNumber){
		this.refNumber = refNumber;
	}
	
	public String getFirstName(){
		return firstName;
	}
	
	public void setFirstName(String firstName){
		this.firstName = firstName;
	}
	
	public String getLastName(){
		return lastName;
	}
	
	public void setLastName(String lastName){
		this.lastName = lastName;
	}
	
	public String getUsername(){
		return username;
	}
	
	public void setUsername(String username){
		this.username = username;
	}
	
	@Override
	public boolean equals(Object o){
		if(o instanceof User){
			User other = (User)o;
			return this.userId.equals(other.getUserId());
		}else{
			return false;
		}
	}
	
	@Override
	public int hashCode(){
		return this.userId.hashCode();
	}
}