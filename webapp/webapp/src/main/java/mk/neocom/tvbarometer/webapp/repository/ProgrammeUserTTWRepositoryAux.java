package mk.neocom.tvbarometer.webapp.repository;

import java.util.Date;
import java.util.List;

import mk.neocom.tvbarometer.webapp.model.entity.ProgrammeUserTTW;

public interface ProgrammeUserTTWRepositoryAux {

	List<ProgrammeUserTTW> calculateTTW(String channel, Date from, Date to);
	
	List<ProgrammeUserTTW> getTTWForUser(String userid, String channel, Date from, Date to);
	
	List<String> getChannels();
}