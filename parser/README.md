Барометар на гледаност на ТВ програма - парсер
==============================================

Содржина
--------

1. [Вовед](#markdown-header-1)
2. [Користење](#markdown-header-2)
3. [Однесување](#markdown-header-3)
4. [Документација](#markdown-header-4)

1. Вовед
--------

Парсерот во проектот „Барометар на гледаност на ТВ програма“ претставува алатка за пресметување на вкупното време на гледање на емисии на ТВ програми од страна на корисниците со анализирање на логовите генерирани од уреди за дигитална телевизија. Софтверот е изграден со Java: Eclipse / Maven проект.

2. Користење
------------

Во `dist` директориумот се наоѓа извршлив JAR од тековната верзија на софтверот. Парсерот се извршува од командна линија (потребна е функционална Јава виртуелна машина):

`java -jar parser.jar`

Претходната инструкција го прикажува корисничкиот интерфејс на парсерот. Истиот може да се користи и од командна линија со специфицирање на потребните аргументи. Подетални инструкции за неговото користење можат да се прегледаат преку менито на корисничкиот интерфејс на самата апликација или со следата команда:
 
`java -jar parser.jar -h`

3. Однесување
-------------

### 3.1 Типови, формат и содржина на статистиките

Парсерот генерира статистики за вкупно време на гледање на емисии на ТВ програми по корисник. Дополнително се генерира документ кој ги содржи влезните ЕПГ информации во формат подобен за нивно понатамошно користење.

Статистиките се запишуваат во соодветно именувани документи во CSV формат:

+ emissions.csv - излезен ЕПГ документ,
+ users/username-stats.csv - статистики за вкупна гледаност на емисии на ТВ програми за корисник со корисничко име `username`.

Генерираните статистики се однесуваат само на каналите за кои се зададени ЕПГ документи. Излезните статистики по корисник содржат записи само за емисиите на ТВ програми кои корисникот ги гледал.

Датум-време записите во излезните статистики се во CEST, т.е. UTC+2 временската зона.

Парсерот овозможува генерирање на `emissions` документот и без парсирање на логови. Во таков случај се уште е потребно да се специфицира `logs` директориум (може да биде празен).

### 3.2 Задавање псевдоними на канали

ЕПГ документите се именуваат во следниот формат: `ChannelName,ChannelAlias1,ChannelAlias2,...-EPG.txt`. При тоа `ChannelName` или барем еден `ChannelAlias` мора да биде еднаков со вредноста на `data` колоната во влезните CSV логови кои се парсираат. Во излезните статистики каналот ќе биде именуван според вредноста на `ChannelName`, а псевдонимите `ChannelAlias` овозможуваат мапирање на повеќе различни вредности на `data` колоната во логовите во ист канал. ЕПГ документот може да биде именуван со празни места после `,` знакот за оддвојување, т.е.: `ChannelName, ChannelAlias1, ChannelAlias2, ...-EPG.txt`, и `ChannelName` или било кој `ChannelAlias` можат да содржат празни места, но името на документот мора да завршува со `-EPG` и истиот да биде TXT документ.

Името на каналот `ChannelName` секогаш добива еден автоматски псевдоним: "ChannelName HD", кој не е потребно да се специфицира мануелно преку додавање на псевдоним во името на ЕПГ документите. Овој псевдоним служи за опфаќање на двете можни варијанти на ТВ канал: HD и не-HD.

### 3.3 Мемориски побарувања

*Напомена: оваа секција беше значајна за верзиите на парсерот кои пресметуваа гледаност по временски интервали. Тековната верзија не извршува такви пресметки, а секцијата е оставена во упатството поради илустрацијата за партиционирање на податочното множество која е корисна не само од аспект на мемориските побарувања на апликацијата.*

Големината на потребниот мемориски простор за пресметување статистики зависи од:

1. бројот на корисници за кои се парсираат логови, и
2. бројот на канали за кои се врши мапирање со ЕПГ документи.

Иако мемориските побарувања на парсерот се целосно оптимизирани, возможно е влезното податочно множество да е премногу големо при што се случува `OutOfMemoryError`. Во таков случај, потребно е партиционирање на влезното множество што може да се изврши преку партиционирање на влезните логови и/или партиционирање на ЕПГ документите. На пример, податочно множество од 1000 корисници и 100 канали може да се партиционира на 10 податочни множества со 100 корисници x 100 канали, 20 податочни множества со 50 корисници x 100 канали, 40 податочни множества со 50 корисници x 50 канали, итн.

4. Документација
----------------

`ParserApp` класата е извршливата влезна точка на парсерот. Истата го создава корисничкиот интерфејс, ги парсира аргументите од командната линија, го конфигурира внатрешниот записник кој е класа во `logger` пакетот, и го извршува соодветниот парсер.

Парсерите се наоѓаат во `parsers` пакетот и се поврзани со `ParserApp` користејќи го интерфејсот `LogParser`.

Имплементацијата на интерфејсот која се користи во парсерот е хиерархијата на класи во `parsers` пакетот со основна класа `DefaultLogParser` и изведена класа `DefaultLogParserImpl`. Хиерархијата постои за да се разграничат функционалностите кои се исти за секој парсер и се наоѓаат во основната класа, како вчитувањето на записите од логовите, со тие кои се специфични за различни пристапи кон пресметувањето на статистиките и се наоѓаат во изведената класа.

- `DefaultLogParser` класата содржи информации за заглавјата во EPG документите, влезните логови во CSV формат и излезните статистики во CSV формат. Класата содржи повеќе помошни методи кои се наменети да се повикуваат од изведените класи. Издвоени се поважните од нив:
    + `MIN_WATCH_TIME_THRESHOLD` - константа која го специфицира минималното времетраење на гледање на програма во секунди кое се зема во предвид при пресметување на статистиките;
    + `Object[] parse(File file)` - статички метод за парсирање на логови кој се повикува од изведените класи. Истиот ги враќа информациите од записите во документот како листа од `LogLine` објекти.

- `DefaultLogParserImpl` класата претставува генератор на статистики. Се пресметува гледаност на емисии на ТВ програми по корисник. Класата го генерира и `emissions` документот кој ги содржи влезните ЕПГ информации во формат подобен за понатамошно користење. Некои поважни структури и методи на класата се:
    + `boolean bindEPGDocument(File file)` - метод кој го вчитува редоследот на емитување на програми на ТВ канал. Редоследот се запишува во податочната структура `programmes` со тип `TreeMap<Channel, TreeSet<ProgrammeRecord>>()` - подредена мапа од канали кон подредено множество од емитувања на програми. Подредувањето на каналите се врши лексички, а подредувањето на емитувањата на програмите хронолошки;
    + `boolean parseFile(File file)` - метод кој го извршува пресметувањето на статистиките повикувајќи го помошниот метод `void calculateStatistics(User user, Channel channel, Date watchStart, Date watchEnd)` за пресметување на вкупна гледаност на емисии од корисник. Статистиките се зачувуваат во податочната структура `statisticsForUsers` од тип `TreeMap<User, TreeMap<ProgrammeRecord, Long>>` - подредена мапа од корисници кон подредени мапи од емисии на програми кон времиња на гледање во секунди;
    + `void writeStatistics(File directory)` - методот кој го извршува запишувањето на статистиките повикувајќи ги помошните методи за запишување на соодветните излезни статистики и ЕПГ информации: `void writeUserStatistics(String dirPath)` и `writeEmissionsList(String dirPath)`.