package mk.neocom.tvbarometer.parser.logger;

public interface Logger{
	void logMessage(String message);
}