package mk.neocom.tvbarometer.parser.logger;

public interface Loggable {
	void log(String message);
}