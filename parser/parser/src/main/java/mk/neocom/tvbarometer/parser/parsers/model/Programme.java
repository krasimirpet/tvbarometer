package mk.neocom.tvbarometer.parser.parsers.model;

public class Programme implements Comparable<Programme>
{
	private Channel channel;
	
	private String name;
	
	public Programme(){}
	
	public Programme(Channel channel, String name)
	{
		this.channel = channel;
		this.name = name;
	}
	
	public Channel getChannel()
	{
		return channel;
	}
	
	public void setChannel(Channel channel)
	{
		this.channel = channel;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}

	@Override
	public boolean equals(Object obj){
		if(obj == null || !(obj instanceof Programme)){
			return false;
		}else{
			Channel objChannel = ((Programme)obj).getChannel();
			String objName = ((Programme)obj).getName();
			if(this.channel != null && objChannel != null){
				if(this.channel.equals(objChannel)){
					if(this.name != null && objName != null){
						return this.name.equalsIgnoreCase(objName);
					}else{
						return false;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
	}
	
	@Override
	public int hashCode(){
		if(this.channel != null && this.name != null){
			return this.name.hashCode() ^ this.channel.hashCode();
		}else if(this.name != null){
			return this.name.hashCode();
		}else if(this.channel != null){
			return this.channel.hashCode();
		}else{
			return 0;
		}
	}
	
	public int compareTo(Programme obj) {
		//lexical sort
		Channel objChannel = ((Programme)obj).getChannel();
		String objName = ((Programme)obj).getName();
		if(this.channel != null && objChannel != null){
			int channelMark = this.channel.compareTo(objChannel);
			if(channelMark == 0){
				if(this.name != null && objName != null){
					return this.name.compareToIgnoreCase(objName);
				}else if(this.name != null && objName == null){
					return 1;
				}else if(this.name == null && objName != null){
					return -1;
				}else{
					return 0;
				}
			}else{
				return channelMark;
			}
		}else if(this.channel != null && objChannel == null){
			return 1;
		}else if(this.channel == null && objChannel != null){
			return -1;
		}else{
			return 0;
		}
	}
}