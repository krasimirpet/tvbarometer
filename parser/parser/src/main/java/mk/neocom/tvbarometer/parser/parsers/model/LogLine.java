package mk.neocom.tvbarometer.parser.parsers.model;

import java.util.Date;

public class LogLine
{
	//the user subject of the record
	private User user;
	
	//record start date
	private Date startDate;
	
	//record end date
	private Date endDate;
	
	//how long did the user watch the channel (= endDate - startDate, in seconds)
	private int duration;
	
	//mac address
	private String macAddress;
	
	//player ip address
	private String playerIp;
	
	//server ip address
	private String serverIp;
	
	//TV channel
	private Channel channel;
	
	//traffic consumption
	private String trafficConsumption;
	
	//user device
	private String device;
	
	//device version
	private String deviceVersion;
	
	//application version
	private String appVersion;
	
	public LogLine(){}
	
	public LogLine(User user, Date startDate, Date endDate, int duration, String device, String macAddress, String playerIp, String serverIp, Channel channel, String trafficConsumption, String deviceVersion, String appVersion)
	{
		this.user = user;
		this.startDate = startDate;
		this.endDate = endDate;
		this.duration = duration;
		this.device = device;
		this.macAddress = macAddress;
		this.playerIp = playerIp;
		this.serverIp = serverIp;
		this.channel = channel;
		this.trafficConsumption = trafficConsumption;
		this.deviceVersion = deviceVersion;
		this.appVersion = appVersion;
	}
	
	public User getUser()
	{
		return user;
	}
	
	public void setUser(User user)
	{
		this.user = user;
	}
	
	public Date getStartDate()
	{
		return startDate;
	}
	
	public void setStartDate(Date startDate)
	{
		this.startDate = startDate;
	}
	
	public Date getEndDate()
	{
		return endDate;
	}
	
	public void setEndDate(Date endDate)
	{
		this.endDate = endDate;
	}
	
	public int getDuration()
	{
		return duration;
	}
	
	public void setDuration(int duration)
	{
		this.duration = duration;
	}
	
	public String getDevice()
	{
		return device;
	}
	
	public void setDevice(String device)
	{
		this.device = device;
	}
	
	public String getMACAddress()
	{
		return macAddress;
	}
	
	public void setMACAddress(String macAddress)
	{
		this.macAddress = macAddress;
	}
	
	public String getPlayerIP()
	{
		return playerIp;
	}
	
	public void setPlayerIP(String playerIp)
	{
		this.playerIp = playerIp;
	}
	
	public String getServerIP()
	{
		return serverIp;
	}
	
	public void setServerIP(String serverIp)
	{
		this.serverIp = serverIp;
	}
	
	public Channel getChannel()
	{
		return channel;
	}
	
	public void setChannel(Channel channel)
	{
		this.channel = channel;
	}
	
	public String getTrafficConsumption()
	{
		return trafficConsumption;
	}
	
	public void setTrafficConsumption(String trafficConsumption)
	{
		this.trafficConsumption = trafficConsumption;
	}
	
	public String getDeviceVersion()
	{
		return deviceVersion;
	}
	
	public void setDeviceVersion(String deviceVersion)
	{
		this.deviceVersion = deviceVersion;
	}
	
	public String getAppVersion()
	{
		return appVersion;
	}
	
	public void setAppVersion(String appVersion)
	{
		this.appVersion = appVersion;
	}
}