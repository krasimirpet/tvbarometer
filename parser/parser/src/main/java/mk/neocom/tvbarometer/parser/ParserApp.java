package mk.neocom.tvbarometer.parser;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.IOException;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

import mk.neocom.tvbarometer.parser.logger.DefaultLogger;
import mk.neocom.tvbarometer.parser.logger.Loggable;
import mk.neocom.tvbarometer.parser.logger.Logger;
import mk.neocom.tvbarometer.parser.utility.Utilities;
import mk.neocom.tvbarometer.parser.parsers.DefaultLogParserImpl;
import mk.neocom.tvbarometer.parser.parsers.LogParser;

public class ParserApp
{
	private static boolean parsingInProgress = false;
	
	private static JTextField textLogs;
	private static JTextField textOut;
	private static JTextField textEpg;
	private static JFileChooser fcLogs;
	private static JFileChooser fcOut;
	private static JFileChooser fcEpg;
	
	private static StatusDialog statusDialog;
	
    public static void main(String[] args)
    {
    	if(args.length == 0){
    		//display UI
    		SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    ParserApp.setUpGUI();
                }
            });
    	}else if(args.length == 1 && args[0].toLowerCase().equals("-h")){
    		//print help
    		System.out.print(ParserApp.getUsage());
    	}else{
    		try{
    			//begin process based on command line parameters
	    		Object[] parameters = ParserApp.parseCommandLineParameters(args);
	    		
	    		File epgDir = (File)parameters[0];
	    		File logsDir = (File)parameters[1];
	    		File outDir = (File)parameters[2];

	    		ParserApp.parse(epgDir, logsDir, outDir);
    		}catch(ParserException exception){
    			System.out.println("Упатство за користење:");
    	        System.out.println();
    			System.out.println(exception.getMessage());
    			return;
    		}
    	}
    	
    	parsingInProgress = false;
    }
    
    private static void setUpGUI()
    {
        final JFrame frame = new JFrame("ТВ Барометар - Парсер");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JMenuBar menuBar = new JMenuBar();
        JMenu menuAbout = new JMenu("За апликацијата"); 
        
        JMenuItem menuItemInstructions = new JMenuItem("Инструкции за користење...");
        menuItemInstructions.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				JOptionPane.showMessageDialog(frame, "<html><body><div style='width:340px;text-align:justify !IMPORTANT;'>" + ParserApp.getUsage().replaceAll("\n", "<br />") + "</div></body></html>", "Инструкции за користење", JOptionPane.INFORMATION_MESSAGE);
			}
        });
        menuAbout.add(menuItemInstructions);
        
        menuBar.add(menuAbout);
        
        frame.setJMenuBar(menuBar);
        
        JPanel inputPanel = new JPanel();
        inputPanel.setBorder(BorderFactory.createTitledBorder("Поддесувања"));
        inputPanel.setLayout(new BoxLayout(inputPanel, BoxLayout.Y_AXIS));
        
        JPanel panelEpg = new JPanel();
        panelEpg.setLayout(new BoxLayout(panelEpg, BoxLayout.X_AXIS));
        
        JLabel labelEpg = new JLabel("ЕПГ/in:");
        textEpg = new JTextField();
        textEpg.setMaximumSize(new Dimension(1000, 26));
        textEpg.setEditable(false);
        JButton buttonEpg = new JButton("Избери...");
        buttonEpg.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				if(fcEpg.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION){
					textEpg.setText(fcEpg.getSelectedFile().getPath());
				}
			}
        });
        fcEpg = new JFileChooser();
        fcEpg.setDialogTitle("Изберете го директориумот кој ги содржи ЕПГ документите:");
        fcEpg.setMultiSelectionEnabled(false);
        fcEpg.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fcEpg.setAcceptAllFileFilterUsed(false);

        panelEpg.add(Box.createHorizontalStrut(4));
        panelEpg.add(labelEpg);
        panelEpg.add(Box.createHorizontalStrut(57));
        panelEpg.add(textEpg);
        panelEpg.add(Box.createHorizontalStrut(2));
        panelEpg.add(buttonEpg);
        
        JPanel panelLogs = new JPanel();
        panelLogs.setLayout(new BoxLayout(panelLogs, BoxLayout.X_AXIS));
        
        JLabel labelLogs = new JLabel("Логови/in:");
        textLogs = new JTextField();
        textLogs.setEditable(false);
        textLogs.setMaximumSize(new Dimension(1000, 26));
        JButton buttonLogs = new JButton("Избери...");
        buttonLogs.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				if(fcLogs.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION){
					textLogs.setText(fcLogs.getSelectedFile().getPath());
				}
			}
        });
        fcLogs = new JFileChooser();
        fcLogs.setDialogTitle("Изберете го директориумот кој ги содржи логовите:");
        fcLogs.setMultiSelectionEnabled(false);
        fcLogs.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fcLogs.setAcceptAllFileFilterUsed(false);
        
        panelLogs.add(Box.createHorizontalStrut(4));
        panelLogs.add(labelLogs);
        panelLogs.add(Box.createHorizontalStrut(39));
        panelLogs.add(textLogs);
        panelLogs.add(Box.createHorizontalStrut(2));
        panelLogs.add(buttonLogs);
        
        JPanel panelOut = new JPanel();
        panelOut.setLayout(new BoxLayout(panelOut, BoxLayout.X_AXIS));
        
        JLabel labelOut = new JLabel("Статистики/out:");
        textOut = new JTextField();
        textOut.setMaximumSize(new Dimension(1000, 26));
        textOut.setEditable(false);
        JButton buttonOut = new JButton("Избери...");
        buttonOut.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				if(fcOut.showOpenDialog(frame) == JFileChooser.APPROVE_OPTION){
					textOut.setText(fcOut.getSelectedFile().getPath());
				}
			}
        });
        fcOut = new JFileChooser();
        fcOut.setDialogTitle("Изберете го излезниот директориум за статистиките:");
        fcOut.setMultiSelectionEnabled(false);
        fcOut.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fcOut.setAcceptAllFileFilterUsed(false);
        
        panelOut.add(Box.createHorizontalStrut(4));
        panelOut.add(labelOut);
        panelOut.add(Box.createHorizontalStrut(8));
        panelOut.add(textOut);
        panelOut.add(Box.createHorizontalStrut(2));
        panelOut.add(buttonOut);
        
        inputPanel.add(panelEpg);
        inputPanel.add(panelLogs);
        inputPanel.add(panelOut);
        
        JPanel actionPanel = new JPanel();
        actionPanel.setBorder(BorderFactory.createTitledBorder("Извршување"));
        
        actionPanel.setLayout(new GridLayout(1, 1));
        
        JPanel actionButtonsContainer = new JPanel();
        actionButtonsContainer.setLayout(new BoxLayout(actionButtonsContainer, BoxLayout.X_AXIS));
        
        JButton executeButton = new JButton("Парсирај");
        executeButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				if(!parsingInProgress){
					final File epgDir = fcEpg.getSelectedFile();
					final File logsDir = fcLogs.getSelectedFile();
					final File outDir = fcOut.getSelectedFile();
					
					if(logsDir != null && outDir != null && epgDir != null){
						SwingUtilities.invokeLater(new Runnable(){
							public void run() {
								if(statusDialog != null){
									statusDialog.dispose();
								}
								statusDialog = new StatusDialog(frame);
								statusDialog.setVisible(true);
								
								ParserApp.parse(epgDir, logsDir, outDir);
							}
						});
					}else{
						JOptionPane.showMessageDialog(frame, "<html><body><div style='width:360px;text-align:justify !IMPORTANT;'>" + "Потребно е најпрво да се избере директориумот каде што се наоѓаат логовите, излезниот директориум за статистиките и ЕПГ документот." + "</div></body></html>", "Грешка", JOptionPane.ERROR_MESSAGE);
					}
				}else{
					SwingUtilities.invokeLater(new Runnable(){
						public void run() {
							statusDialog.setVisible(true);
							
							JOptionPane.showMessageDialog(frame, "<html><body><div style='width:290px;text-align:justify !IMPORTANT;'>" + "Потребно е тековното парсирање да заврши пред да се почне со ново." + "</div></body></html>", "Грешка", JOptionPane.ERROR_MESSAGE);
						}
					});
				}
			}
        });
        
        JButton resetButton = new JButton("Ресетирај");
        resetButton.addActionListener(new ActionListener(){
			public void actionPerformed(ActionEvent evt) {
				fcLogs.setSelectedFile(null);
				fcOut.setSelectedFile(null);
				fcEpg.setSelectedFile(null);
				
				textLogs.setText("");
				textOut.setText("");
				textEpg.setText("");
			}
        });
        
        actionButtonsContainer.add(Box.createHorizontalGlue());
        actionButtonsContainer.add(executeButton);
        actionButtonsContainer.add(Box.createHorizontalStrut(5));
        actionButtonsContainer.add(resetButton);
        actionButtonsContainer.add(Box.createHorizontalGlue());
        
        actionPanel.add(actionButtonsContainer);
        
        Container contentPane = frame.getContentPane();
        contentPane.setLayout(new BorderLayout());
        
        contentPane.add(inputPanel);
        contentPane.add(actionPanel, BorderLayout.SOUTH);

        frame.setSize(new Dimension(650, 260));
        frame.setVisible(true);
    }
   
    private static void parse(File epgDir, File logsDir, File outDir)
    {
    	parsingInProgress = true;
    	
    	final File finalEpgDir = epgDir;
    	final File finalLogsDir = logsDir;
    	final File finalOutDir = outDir;
    	
    	new Thread(new Runnable(){
			public void run(){
				LogParser parser = new DefaultLogParserImpl();
				Logger logger = new DefaultLogger(statusDialog);
				
				File[] epgFiles = Utilities.getFilesInDirectoryWithExtension(finalEpgDir, "txt");
				File[] files = Utilities.getFilesInDirectoryWithExtension(finalLogsDir, "csv");
				
				if(epgFiles.length > 0){
					logger.logMessage("Вчитување на ЕПГ документите:\n");
					for(File file : epgFiles){
						try{
							logger.logMessage(file.getName());
							if(parser.bindEPGDocument(file)){
								logger.logMessage(" - ОК\n");
							}else{
								logger.logMessage(" - невалиден ЕПГ документ\n");
							}
						}catch(IOException exception){
							logger.logMessage(" - грешка при вчитувањето\n");
						}
					}
					logger.logMessage("\n");
					logger.logMessage("Парсирање на логовите:\n");
					for(File file : files){
						try{
							logger.logMessage(file.getName());
							if(parser.parseFile(file)){
								logger.logMessage(" - ОК\n");
							}else{
								logger.logMessage(" - невалиден документ\n");
							}
						}catch(IOException exception){
							logger.logMessage(" - грешка при вчитувањето\n");
						}
					}
					
					logger.logMessage("\n");
					logger.logMessage("Запишување на статистиките во " + finalOutDir.getAbsolutePath() + ".\n");
					try{
						parser.writeStatistics(finalOutDir);
						logger.logMessage("\n");
						logger.logMessage("Готово");
					}catch(IOException exception){
						logger.logMessage("Грешка при зачувувањето на статистиките.\n");
					}
				}else{
					logger.logMessage("Не постојат ЕПГ документи во наведениот директориум.\n");
				}
				
				parsingInProgress = false;
			}
    	}).start();
    }
    
    private static Object[] parseCommandLineParameters(String[] args) throws ParserException
    {
    	String epgDirLocation = null;
        String logsDirLocation = null;
        String outDirLocation = null;
        
        boolean argsMode = false;
        int currentParam = -1; //0 is -logs, 1 is -out, 2 is -epg
        for(int i = 0; i < args.length; i++)
        {
            if(!argsMode){
                //args[i] contains parameter
                if(epgDirLocation == null && args[i].toLowerCase().equals("-epg")){
                	currentParam = 0;
                    argsMode = true;
                }else if(logsDirLocation == null && args[i].toLowerCase().equals("-log")){
                	currentParam = 1;
                    argsMode = true;
                }else if(outDirLocation == null && args[i].toLowerCase().equals("-out")){
                	currentParam = 2;
                    argsMode = true;
                }
            }else{
                //args[i] contains parameter value
                if(currentParam == 0){
                	epgDirLocation = args[i];
                    argsMode = false;
                }else if(currentParam == 1){
                	logsDirLocation = args[i];
                    argsMode = false;
                }else if(currentParam == 2){
                	outDirLocation = args[i];
                	argsMode = false;
                }
            }
        }
        
        File epgDir = new File(epgDirLocation);
        File logsDir = new File(logsDirLocation);
        File outDir = new File(outDirLocation);
		if(logsDir.isDirectory() && outDir.isDirectory() && epgDir.isDirectory()){
			return new Object[]{epgDir, logsDir, outDir};
		}else{
			throw new ParserException("Грешка во параметри на конзолна линија: -epg, -log и -out мора да посочуваат постоечки директориуми.");
		}
    }
    
    private static String getUsage()
    {
    	StringBuilder builder = new StringBuilder();
    	
        builder.append("Парсерот може да се користи преку графичкиот кориснички интерфејс, или од командна линија со следните параметри:");
        builder.append("\n\n");
        builder.append("-epg: локација на директориумот каде што се наоѓаат ЕПГ документите во txt формат.");
        builder.append("\n");
        builder.append("-log: локација на директориумот каде што се наоѓаат логовите (се парсираат сите .csv фајлови во директориумот).");
        builder.append("\n");
        builder.append("-out: локација на директориум за зачувување на генерираните статистики.");
        builder.append("\n");
        
        return builder.toString();
    }
    
    private static class StatusDialog extends JDialog implements Loggable {
    	private JTextArea textArea;
    	
    	public StatusDialog(JFrame parent){
    		super(parent, false);	//modeless
    		
    		textArea = new JTextArea();
    		textArea.setEditable(false);
    		textArea.setLineWrap(true);
    		textArea.setWrapStyleWord(true);
    		textArea.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(Color.GRAY), BorderFactory.createEmptyBorder(1, 4, 1, 4)));
    		textArea.setBackground(new Color(220, 220, 220));
    		
    		JScrollPane textAreaScrollPane = new JScrollPane(textArea);
    		
    		JButton closeButton = new JButton("Затвори");
    		closeButton.setAlignmentX(CENTER_ALIGNMENT);
    		final StatusDialog thisDialog = this;
    		closeButton.addActionListener(new ActionListener(){
				public void actionPerformed(ActionEvent event){
					thisDialog.setVisible(false);
				}
    		});
    		
    		JPanel innerContainer = new JPanel();
    		innerContainer.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
    		innerContainer.setLayout(new BoxLayout(innerContainer, BoxLayout.Y_AXIS));
    		
    		innerContainer.add(textAreaScrollPane);
    		innerContainer.add(Box.createVerticalStrut(5));
    		innerContainer.add(closeButton);
    		
    		this.add(innerContainer, BorderLayout.CENTER);
    		
    		this.setTitle("Статус");
    		this.setSize(new Dimension(300, 400));
    	}
    	
    	public void log(String message){
    		final String finalMessage = message;
    		
    		SwingUtilities.invokeLater(new Runnable(){
				public void run(){
					textArea.setText(textArea.getText() + finalMessage);
				}
			});
    	} 
    }
}
