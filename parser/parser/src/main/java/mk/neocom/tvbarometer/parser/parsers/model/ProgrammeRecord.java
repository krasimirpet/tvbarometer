package mk.neocom.tvbarometer.parser.parsers.model;

import java.util.Date;

public class ProgrammeRecord implements Comparable<ProgrammeRecord>
{
	private Programme programme;
	
	private Date programmeStart;
	
	private Date programmeEnd;
	
	public ProgrammeRecord(){}
	
	public ProgrammeRecord(Programme programme, Date programmeStart, Date programmeEnd)
	{
		this.programme = programme;
		this.programmeStart = programmeStart;
		this.programmeEnd = programmeEnd;
	}
	
	public Programme getProgramme()
	{
		return programme;
	}
	
	public void setProgramme(Programme programme)
	{
		this.programme = programme;
	}
	
	public Date getProgrammeStart()
	{
		return programmeStart;
	}
	
	public void setProgrammeStart(Date programmeStart)
	{
		this.programmeStart = programmeStart;
	}
	
	public Date getProgrammeEnd()
	{
		return programmeEnd;
	}
	
	public void setProgrammeEnd(Date programmeEnd)
	{
		this.programmeEnd = programmeEnd;
	}

	@Override
	public boolean equals(Object obj){
		if(obj == null || !(obj instanceof ProgrammeRecord)){
			return false;
		}else{
			Programme objProgramme = ((ProgrammeRecord)obj).getProgramme();
			Date objProgrammeStart = ((ProgrammeRecord)obj).getProgrammeStart();
			Date objProgrammeEnd = ((ProgrammeRecord)obj).getProgrammeEnd();
			if(this.programme != null && objProgramme != null){
				if(this.programme.equals(objProgramme)){
					if(this.programmeStart != null && objProgrammeStart != null && this.programmeEnd != null && objProgrammeEnd != null){
						return this.programmeStart.equals(objProgrammeStart) && this.programmeEnd.equals(objProgrammeEnd);
					}else{
						return false;
					}
				}else{
					return false;
				}
			}else{
				return false;
			}
		}
	}
	
	@Override
	public int hashCode(){
		if(this.programme != null){
			if(this.programmeStart != null && this.programmeEnd != null){
				return this.programme.hashCode() ^ this.programmeStart.hashCode() ^ this.programmeEnd.hashCode();
			}else if(this.programmeStart != null){
				return this.programme.hashCode() ^ this.programmeStart.hashCode();
			}else if(this.programmeEnd != null){
				return this.programme.hashCode() ^ this.programmeEnd.hashCode();
			}else{
				return this.programme.hashCode();
			}
		}else{
			return 0;
		}
	}
	
	public int compareTo(ProgrammeRecord obj) {
		//sort by (channel, start date)
		Programme objProgramme = ((ProgrammeRecord)obj).getProgramme();
		Date objProgrammeStart = ((ProgrammeRecord)obj).getProgrammeStart();
		if(this.programme != null && objProgramme != null){
			Channel programmeChannel = this.programme.getChannel();
			Channel objProgrammeChannel = objProgramme.getChannel();
			if(programmeChannel != null && objProgrammeChannel != null){
				int channelMark = this.programme.getChannel().compareTo(objProgrammeChannel);
				if(channelMark == 0){
					if(this.programmeStart != null && objProgrammeStart != null){
						return this.programmeStart.compareTo(objProgrammeStart);
					}else if(this.programmeStart != null && objProgrammeStart == null){
						return 1;
					}else if(this.programmeStart == null && objProgrammeStart != null){
						return -1;
					}else{
						return 0;
					}
				}else{
					return channelMark;
				}
			}else if(programmeChannel != null && objProgrammeChannel == null){
				return 1;
			}else if(programmeChannel == null && objProgrammeChannel != null){
				return -1;
			}else{
				return 0;
			}
		}else if(this.programme != null && objProgramme == null){
			return 1;
		}else if(this.programme == null && objProgramme != null){
			return -1;
		}else{
			return 0;
		}
	}
}