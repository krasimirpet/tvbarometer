package mk.neocom.tvbarometer.parser.parsers.model;

public class Channel implements Comparable<Channel>
{
	private String name;
	private String[] aliases;
	
	public Channel(){}
	
	public Channel(String name, String[] aliases)
	{
		this.name = name;
		this.aliases = aliases;
	}
	
	public String getName()
	{
		return name;
	}
	
	public void setName(String name)
	{
		this.name = name;
	}
	
	public String[] getAliases()
	{
		return aliases;
	}
	
	public void setAliases(String[] aliases)
	{
		this.aliases = aliases;
	}
	
	@Override
	public boolean equals(Object obj){
		if(obj == null || !(obj instanceof Channel)){
			return false;
		}else{
			String objName = ((Channel)obj).getName();
			String[] objAliases = ((Channel)obj).getAliases();
			
			if(this.name != null && objName != null){ 
				if(this.name.equalsIgnoreCase(objName)){
					return true;
				}else if((this.name + " hd").equalsIgnoreCase(objName) || this.name.equalsIgnoreCase(objName + " hd")){
					return true;
				}else{
					if(this.aliases != null){
						for(int i = 0; i < this.aliases.length; i++){
							if(this.aliases[i].equalsIgnoreCase(objName) || (this.aliases[i] + " hd").equalsIgnoreCase(objName) || this.aliases[i].equalsIgnoreCase(objName + " hd")){
								return true;
							}
						}
					}
					if(objAliases != null){
						for(int i = 0; i < objAliases.length; i++){
							if(objAliases[i].equalsIgnoreCase(this.name) || (objAliases[i] + " hd").equalsIgnoreCase(this.name) || objAliases[i].equalsIgnoreCase(this.name + " hd")){
								return true;
							}
						}
					}
					
					return false;
				}
			}else{
				return false;
			}
		}
	}
	
	@Override
	public int hashCode(){
		if(this.name != null){
			return this.name.hashCode();
		}else{
			return 0;
		}
	} 

	public int compareTo(Channel obj) {
		if(obj != null){
			String objName = obj.getName();
			
			if(this.name != null && objName != null){
				if(this.equals(obj)){
					return 0;
				}else{
					return this.name.compareToIgnoreCase(objName);
				}
			}else if(this.name != null && objName == null){
				return 1;
			}else if(this.name == null && objName != null){
				return -1;
			}else{
				return 0;
			}
		}else{
			return 1;
		}
	} 
}