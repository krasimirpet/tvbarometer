package mk.neocom.tvbarometer.parser.parsers.model;

public class User implements Comparable<User>
{
	//effective id in parser
	private String userId;
	
	private String accountId;
	
	private String refNumber;
	
	private String firstName;
	
	private String lastName;
	
	private String username;
	
	public User(){}
	
	public User(String userId, String accountId, String refNumber, String firstName, String lastName, String username)
	{
		this.userId = userId;
		this.accountId = accountId;
		this.refNumber = refNumber;
		this.firstName = firstName;
		this.lastName = lastName;
		this.username = username;
	}
	
	public String getUserId()
	{
		return userId;
	}
	
	public void setUserId(String userId)
	{
		this.userId = userId;
	}
	
	public String getAccountId()
	{
		return accountId;
	}
	
	public void setAccountId(String accountId)
	{
		this.accountId = accountId;
	}
	
	public String getRefNumber()
	{
		return refNumber;
	}
	
	public void setRefNumber(String refNumber)
	{
		this.refNumber = refNumber;
	}
	
	public String getFirstName()
	{
		return firstName;
	}
	
	public void setFirstName(String firstName)
	{
		this.firstName = firstName;
	}
	
	public String getLastName()
	{
		return lastName;
	}
	
	public void setLastName(String lastName)
	{
		this.lastName = lastName;
	}
	
	public String getUsername()
	{
		return username;
	}
	
	public void setUsername(String username)
	{
		this.username = username;
	}

	@Override
	public boolean equals(Object obj){
		if(obj == null || !(obj instanceof User)){
			return false;
		}else{
			String objId = ((User)obj).getUserId();
			if(this.userId != null && objId != null){
				return this.userId.equals(objId);
			}else{
				return false;
			}
		}
	}
	
	@Override
	public int hashCode(){
		if(this.userId != null){
			return this.userId.hashCode();
		}else{
			return 0;
		}
	}
	
	public int compareTo(User obj) {
		String objId = ((User)obj).getUserId();
		if(this.userId != null && objId != null){
			return this.userId.compareTo(objId);
		}else if(this.userId != null && objId == null){
			return 1;
		}else if(this.userId == null && objId != null){
			return -1;
		}else{
			return 0;
		}
	}
}