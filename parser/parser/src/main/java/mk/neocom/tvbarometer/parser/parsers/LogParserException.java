package mk.neocom.tvbarometer.parser.parsers;

public class LogParserException extends Exception
{
	public LogParserException(){
		super();
	}
	
	public LogParserException(String message)
	{
		super(message);
	}
}