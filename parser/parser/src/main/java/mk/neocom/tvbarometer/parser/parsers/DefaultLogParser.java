package mk.neocom.tvbarometer.parser.parsers;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
//import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
//import java.util.TimeZone;

import mk.neocom.tvbarometer.parser.parsers.model.Channel;
import mk.neocom.tvbarometer.parser.parsers.model.LogLine;
import mk.neocom.tvbarometer.parser.parsers.model.User;

public class DefaultLogParser {
	protected static class HeaderConstants {
		//logs
		public static final String USERID = "userId";
		public static final String ACCOUNTID = "accountId";
		public static final String REFNUMBER = "refNumber";
		public static final String FIRSTNAME = "firstName";
		public static final String LASTNAME = "lastName";
		public static final String USERNAME = "username";
		public static final String STARTDATE = "startDate";
		public static final String ENDDATE = "endDate";
		public static final String DURATION = "duration(in seconds)";
		public static final String DEVICE = "device";
		public static final String MACADDRESS = "macAddress";
		public static final String PLAYERIP = "playerIp";
		public static final String SERVERIP = "serverIp";
		public static final String DATA = "data";
		public static final String TRAFFICCONSUMPTION = "trafficConsumptionInMB";
		public static final String DEVICEVERSION = "deviceVersion";
		public static final String APPVERSION = "appVersion";
		
		//epg
		public static final String EPGSTARTTIME = "ПОЧЕТНО ВРЕМЕ";
		public static final String EPGSTARTDATE = "ПОЧЕТНА ДАТА";
		public static final String EPGENDDATE = "КРАЈНА ДАТА";
		public static final String EPGDURATION = "ВРЕМЕТРАЕЊЕ";
		public static final String EPGTITLE = "НАСЛОВ";
		
		//generated statistics
		public static final String CHANNEL = "channel";
		public static final String PROGRAMME = "programme";
		public static final String PROGRAMMERECORD = "programme emission";
		public static final String PROGRAMMERECORD_START = "programme start";
		public static final String PROGRAMMERECORD_END = "programme end";
		public static final String TIME_MINUTES = "time (minutes)";
		public static final String TIME_HOURS = "time (hours)";
	}
	
	protected static final int MIN_WATCH_TIME_THRESHOLD = 5; 	//specifies the minimum amount of time in seconds (exclusive) that the user spends in watching a channel / programme affecting the statistics
	
	protected static final String USER_STATISTICS_DIRECTORY_NAME = "users";
	
	protected static final char LOG_TEXT_DELIMITER = '"';
	
	protected static List<LogLine> parse(File file) throws IOException
	{
		List<String> lines = Files.readAllLines(file.toPath(), Charset.forName("UTF-8"));
		Iterator<String> iterLines = lines.iterator();
		
		if(iterLines.hasNext()){
			//read header
			List<String> headerParts = DefaultLogParser.splitLine(iterLines.next());
			final int headerPartsSize = headerParts.size();
			
			int indexUserId = -1, indexAccountId = -1, indexRefNumber = -1, indexFirstName = -1, indexLastName = -1, indexUsername = -1, indexStartDate = -1, indexEndDate = -1, indexDuration = -1, indexDevice = -1, indexMacAddress = -1, indexPlayerIp = -1, indexServerIp = -1, indexData = -1, indexTrafficConsumption = -1, indexDeviceVersion = -1, indexAppVersion = -1;
			for(int i = 0; i < headerPartsSize; i++){
				String normalizedHeaderPart = DefaultLogParser.normalizeLinePart(headerParts.get(i));
				
				if(normalizedHeaderPart.equals(HeaderConstants.USERID)){
					indexUserId = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.ACCOUNTID)){
					indexAccountId = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.REFNUMBER)){
					indexRefNumber = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.FIRSTNAME)){
					indexFirstName = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.LASTNAME)){
					indexLastName = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.USERNAME)){
					indexUsername = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.STARTDATE)){
					indexStartDate = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.ENDDATE)){
					indexEndDate = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.DURATION)){
					indexDuration = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.DEVICE)){
					indexDevice = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.MACADDRESS)){
					indexMacAddress = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.PLAYERIP)){
					indexPlayerIp = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.SERVERIP)){
					indexServerIp = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.DATA)){
					indexData = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.TRAFFICCONSUMPTION)){
					indexTrafficConsumption = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.DEVICEVERSION)){
					indexDeviceVersion = i;
				}else if(normalizedHeaderPart.equals(HeaderConstants.APPVERSION)){
					indexAppVersion = i;
				}
			}
				
			if(indexUserId == -1 || indexAccountId == -1 || indexRefNumber == -1 || indexFirstName == -1 || indexLastName == -1 || indexUsername == -1 || indexStartDate == -1 || indexEndDate == -1 || indexDuration == -1 || indexDevice == -1 || indexMacAddress == -1 || indexPlayerIp == -1 || indexServerIp == -1 || indexData == -1 || indexTrafficConsumption == -1 || indexDeviceVersion == -1 || indexAppVersion == -1){
				//invalid file
				return null;
			}
			
			//read lines
			List<LogLine> linesInMemory = new LinkedList<LogLine>();
			
			while(iterLines.hasNext()){
				try{
					List<String> lineParts = DefaultLogParser.splitLine(iterLines.next());
					if(lineParts.size() != headerPartsSize){
						continue;
					}
					
					String userId = DefaultLogParser.normalizeLinePart(lineParts.get(indexUserId));
					String accountId = DefaultLogParser.normalizeLinePart(lineParts.get(indexAccountId));
					String refNumber = DefaultLogParser.normalizeLinePart(lineParts.get(indexRefNumber));
					String firstName = DefaultLogParser.normalizeLinePart(lineParts.get(indexFirstName));
					String lastName = DefaultLogParser.normalizeLinePart(lineParts.get(indexLastName));
					String username = DefaultLogParser.normalizeLinePart(lineParts.get(indexUsername));
					User user = new User(userId, accountId, refNumber, firstName, lastName, username);
					
					Date timestampStart = DefaultLogParser.parseLogTimestamp(DefaultLogParser.normalizeLinePart(lineParts.get(indexStartDate)));
					Date timestampEnd = DefaultLogParser.parseLogTimestamp(DefaultLogParser.normalizeLinePart(lineParts.get(indexEndDate)));					
					int duration = Integer.parseInt(DefaultLogParser.normalizeLinePart(lineParts.get(indexDuration)));
					String device = DefaultLogParser.normalizeLinePart(lineParts.get(indexDevice));
					String macAddress = DefaultLogParser.normalizeLinePart(lineParts.get(indexMacAddress));
					String playerIp = DefaultLogParser.normalizeLinePart(lineParts.get(indexPlayerIp));
					String serverIp = DefaultLogParser.normalizeLinePart(lineParts.get(indexServerIp));
					
					String channelName = DefaultLogParser.normalizeLinePart(lineParts.get(indexData));
					Channel channel = new Channel(channelName, null); 
					
					String trafficConsumption = DefaultLogParser.normalizeLinePart(lineParts.get(indexTrafficConsumption));
					String deviceVersion = DefaultLogParser.normalizeLinePart(lineParts.get(indexDeviceVersion));
					String appVersion = DefaultLogParser.normalizeLinePart(lineParts.get(indexAppVersion));
					
					if(duration > DefaultLogParser.MIN_WATCH_TIME_THRESHOLD){
						linesInMemory.add(new LogLine(user, timestampStart, timestampEnd, duration, device, macAddress, playerIp, serverIp, channel, trafficConsumption, deviceVersion, appVersion));
					}
				}catch(IllegalArgumentException e){}	//skip the line
			}
			
			return linesInMemory;
		}else{
			return null;
		}
	}
	
	protected static Date parseLogTimestamp(String timestamp) throws IllegalArgumentException
	{
		try{
			return new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.ENGLISH).parse(timestamp);
		}catch(ParseException e){
			throw new IllegalArgumentException("invalid timestamp");
		}
	} 
	
	protected static String generateStatTimestamp(Date datetime, boolean time) throws IllegalArgumentException
	{
		return new SimpleDateFormat("dd MMM yyyy" + (time ? " HH:mm:ss" : ""), Locale.ENGLISH).format(datetime);
	}
	
	private static List<String> splitLine(String line){
		List<String> parts = new ArrayList<String>(10);
		
		int length = line.length();
		int startIndex = 0;
		boolean inQuotes = false;
		for(int i = 0; i < length; i++){
			if(line.charAt(i) == DefaultLogParser.LOG_TEXT_DELIMITER){
				inQuotes = !inQuotes;
			}else if(line.charAt(i) == ',' && !inQuotes){
				parts.add(line.substring(startIndex, i));
				startIndex = i + 1;
			}
		}
		parts.add(line.substring(startIndex, length));
		
		return parts;
	} 
	
	private static String normalizeLinePart(String linePart)
	{
		//trims quotes at the beginning and end of the line part
		boolean startsWithQuote = linePart.startsWith("\"");
		boolean endsWithQuote = linePart.endsWith("\"");
		
		if(startsWithQuote && endsWithQuote){
			linePart = linePart.substring(1, linePart.length() - 1);
		}else if(startsWithQuote){
			linePart = linePart.substring(1);
		}else if(endsWithQuote){
			linePart = linePart.substring(0, linePart.length() - 1);
		}
		
		return linePart;
	}
	
	protected static long getTimeDifferenceBetweenDates(Date date1, Date date2){
		return Math.abs(Math.round((date2.getTime() - date1.getTime()) / 1000.0));
	}
}