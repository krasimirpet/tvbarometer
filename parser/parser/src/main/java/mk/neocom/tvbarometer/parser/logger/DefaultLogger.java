package mk.neocom.tvbarometer.parser.logger;

public class DefaultLogger implements Logger {
	private Loggable output;
	
	public DefaultLogger(Loggable output){
		this.output = output;
	}
	
	public void logMessage(String message){
		if(this.output != null){				
			this.output.log(message);
		}else{
			System.out.print(message);
		}
	}
	
}