package mk.neocom.tvbarometer.parser.parsers;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.TreeSet;

import mk.neocom.tvbarometer.parser.parsers.model.Channel;
import mk.neocom.tvbarometer.parser.parsers.model.LogLine;
import mk.neocom.tvbarometer.parser.parsers.model.Programme;
import mk.neocom.tvbarometer.parser.parsers.model.ProgrammeRecord;
import mk.neocom.tvbarometer.parser.parsers.model.User;

public class DefaultLogParserImpl extends DefaultLogParser implements LogParser
{
	private TreeMap<Channel, TreeSet<ProgrammeRecord>> programmes = new TreeMap<Channel, TreeSet<ProgrammeRecord>>();
	
	private Map<User, TreeMap<ProgrammeRecord, Long>> statisticsForUsers = new TreeMap<User, TreeMap<ProgrammeRecord, Long>>();
	
	public DefaultLogParserImpl(){}
	
	public boolean bindEPGDocument(File file) throws IOException {
		Channel channel = null;
		String filename = file.getName(); //filename must be formatted as: ChannelName,Channel Alias 1,Channel Alias 2,...-EPG.txt, ChannelName (or one of the Channel Aliases) needs to match the 'data' column in the log files (case insensitive, spaces before and after the comma separator are permitted).
		
		if(filename.endsWith("-EPG.txt")){
			String[] filenameParts = filename.substring(0, filename.length() - 8).split(",");
			
			String name = filenameParts[0];
			String[] aliases = new String[filenameParts.length - 1];
			for(int i = 0; i < aliases.length; i++){
				aliases[i] = filenameParts[i + 1].trim();
			}
			
			channel = new Channel(name, aliases);
		}else{
			return false;
		}
		
		List<String> lines = Files.readAllLines(file.toPath(), Charset.forName("UTF-8"));
		Iterator<String> iterLines = lines.iterator();
		
		if(iterLines.hasNext()){
			//read header
			String[] headerParts = iterLines.next().split("\\t+");
			
			int indexStartTime = -1, indexStartDate = -1, indexEndDate = -1, indexDuration = -1, indexTitle = -1;
			for(int i = 0; i < headerParts.length; i++){
				String headerPart = headerParts[i];
				
				if(headerPart.equals(HeaderConstants.EPGSTARTTIME)){
					indexStartTime = i;
				}else if(headerPart.equals(HeaderConstants.EPGSTARTDATE)){
					indexStartDate = i;
				}else if(headerPart.equals(HeaderConstants.EPGENDDATE)){
					indexEndDate = i;
				}else if(headerPart.equals(HeaderConstants.EPGDURATION)){
					indexDuration = i;
				}else if(headerPart.equals(HeaderConstants.EPGTITLE)){
					indexTitle = i;
				}
			}
				
			if(indexStartTime == -1 || indexStartDate == -1 || indexEndDate == -1 || indexDuration == -1 || indexTitle == -1){
				//invalid file
				return false;
			}
			
			//read lines
			while(iterLines.hasNext()){
				try{
					String[] lineParts = iterLines.next().split("\\t+");
					if(lineParts.length != headerParts.length){
						continue;
					}
					
					Date startDate = DefaultLogParserImpl.parseEPGTimestamp(lineParts[indexStartDate] + " " + lineParts[indexStartTime]);
					Date endDate = DefaultLogParserImpl.parseEPGTimestamp(lineParts[indexEndDate]);
					//String duration = lineParts[indexDuration]; //not used
					String title = lineParts[indexTitle];
					
					ProgrammeRecord programmeRecord = new ProgrammeRecord(new Programme(channel, title), startDate, endDate);
					
					if(!programmes.containsKey(channel)){
						programmes.put(channel, new TreeSet<ProgrammeRecord>());
					}
					programmes.get(channel).add(programmeRecord);
				}catch(IllegalArgumentException e){}	//skip the line
			}
			
			return true;
		}else{
			return false;
		}
	}
	
	public boolean parseFile(File file) throws IOException {
		List<LogLine> lines = DefaultLogParser.parse(file);
		
		Iterator<LogLine> iterLines = lines.iterator();
		while(iterLines.hasNext()){
			LogLine line = iterLines.next();
			User user = line.getUser();
			Channel channel = line.getChannel();
			Date watchStart = line.getStartDate();
			Date watchEnd = line.getEndDate();
			
			this.calculateStatistics(user, channel, watchStart, watchEnd);
		}
		
		return true;
	}
	
	private void calculateStatistics(User user, Channel channel, Date watchStart, Date watchEnd){
		//set up user statistics period
		TreeMap<ProgrammeRecord, Long> statisticsForUser = null;
		if(!statisticsForUsers.containsKey(user)){
			statisticsForUser = new TreeMap<ProgrammeRecord, Long>();
			statisticsForUsers.put(user, statisticsForUser);
		}else{
			statisticsForUser = statisticsForUsers.get(user);
		}
		
		//update statistics
		if(programmes.containsKey(channel) && watchStart != null && watchEnd != null){
			Set<ProgrammeRecord> programmesOnChannel = programmes.get(channel);
			for(ProgrammeRecord programmeRecord : programmesOnChannel){
				Date programmeStart = programmeRecord.getProgrammeStart();
				Date programmeEnd = programmeRecord.getProgrammeEnd();

				if(programmeStart != null && programmeEnd != null){
					if(watchEnd.compareTo(programmeStart) <= 0){	//ended watching before programme started
						break;	//current iteration is after watched programmes, break to optimize
					}else if(watchStart.compareTo(programmeEnd) >= 0){	//started watching after programme ended
						continue;	//current iteration is before watched programmes 
					}else{
						//started watching before programme ended and ended watching after programme started
						//watchEnd.compareTo(programmeStart) > 0 && watchStart.compareTo(programmeEnd) < 0
						long durationInSeconds = 0L;
						
						if(watchStart.compareTo(programmeStart) >= 0 && watchEnd.compareTo(programmeEnd) >= 0){
							//started watching after the programme started, stopped watching after the programme ended
							//this programme was watched (programmeEnd - watchStart) seconds, i.e. partial to the end
							
							durationInSeconds = DefaultLogParser.getTimeDifferenceBetweenDates(programmeEnd, watchStart);
						}else if(watchStart.compareTo(programmeStart) >= 0 && watchEnd.compareTo(programmeEnd) < 0){
							//started watching after the programme started, stopped watching before the programme ended
							//this programme was watched (watchEnd - watchStart) seconds, i.e. some part in the middle
							
							durationInSeconds = DefaultLogParser.getTimeDifferenceBetweenDates(watchEnd, watchStart);
						}else if(watchStart.compareTo(programmeStart) < 0 && watchEnd.compareTo(programmeEnd) >= 0){
							//started watching before the programme started, stopped watching after the programme ended
							//this programme was watched (programmeEnd - programmeStart) seconds, i.e. from start to finish
							
							durationInSeconds = DefaultLogParser.getTimeDifferenceBetweenDates(programmeEnd, programmeStart);
						}else if(watchStart.compareTo(programmeStart) < 0 && watchEnd.compareTo(programmeEnd) < 0){
							//started watching before the programme started, stopped watching before the programme ended
							//this programme was watched (watchEnd - programmeStart) seconds, i.e. partial from the start
							
							durationInSeconds = DefaultLogParser.getTimeDifferenceBetweenDates(watchEnd, programmeStart);
						}
						
						//update user statistics
						Long userSecondsTotal = 0L;
						if(statisticsForUser.containsKey(programmeRecord)){
							userSecondsTotal = statisticsForUser.get(programmeRecord);
						}
						userSecondsTotal += durationInSeconds;
						statisticsForUser.put(programmeRecord, userSecondsTotal);
					}
				}
			}
		}
	}

	public void writeStatistics(File directory) throws IOException {
		final String dirFilePath = directory.getPath();
		
		this.writeEmissionsList(dirFilePath);
		this.writeUserStatistics(dirFilePath);
	}
	
	private void writeEmissionsList(String dirPath) throws IOException
	{
		File statisticsFile = new File(dirPath + File.separator + "emissions.csv");
		if(statisticsFile.exists()){
			statisticsFile.delete();
		}
		
		Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(statisticsFile), Charset.forName("UTF-8")));
	    writer.write(this.generateEmissionsList());
	    writer.close();
	}
	
	private String generateEmissionsList()
	{
		StringBuffer emissionsFileContents = new StringBuffer();
		
		//print header
		emissionsFileContents.append(String.format(Locale.ENGLISH, "\"%s\",\"%s\",\"%s\",\"%s\"", HeaderConstants.CHANNEL, HeaderConstants.PROGRAMMERECORD_START, HeaderConstants.PROGRAMMERECORD_END, HeaderConstants.PROGRAMMERECORD));
		
		//print emissions
		for(Channel channel : programmes.keySet()){
			TreeSet<ProgrammeRecord> records = programmes.get(channel);
			for(ProgrammeRecord record : records){
				emissionsFileContents.append("\n" + String.format(Locale.ENGLISH, "\"%s\",%s,%s,\"%s\"", channel.getName(), super.generateStatTimestamp(record.getProgrammeStart(), true), super.generateStatTimestamp(record.getProgrammeEnd(), true), record.getProgramme().getName()));
			}
		}
		
		return emissionsFileContents.toString();
	}
	
	private void writeUserStatistics(String dirPath) throws IOException
	{
		String baseDirPath = dirPath + File.separator + DefaultLogParser.USER_STATISTICS_DIRECTORY_NAME;
		
		File baseDirFile = new File(baseDirPath);
		if(!baseDirFile.exists() && !baseDirFile.mkdir()){
			throw new IOException("Can not create \"" + DefaultLogParser.USER_STATISTICS_DIRECTORY_NAME + "\" directory.");
		}
		
		for(User user : statisticsForUsers.keySet()){
			File userStatisticsFile = new File(baseDirPath + File.separator + user.getUsername() + "-stats.csv");
			if(userStatisticsFile.exists()){
				userStatisticsFile.delete();
			}
			
			Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(userStatisticsFile), Charset.forName("UTF-8")));
		    writer.write(this.generateUserStatistics(user));
		    writer.close();
		}
	}
	
	protected String generateUserStatistics(User user){
		StringBuffer userStatisticsFileContents = new StringBuffer();
		
		final String userid = user.getUserId();
		final String accountid = user.getAccountId();
		final String refnumber = user.getRefNumber();
		final String firstname = user.getFirstName();
		final String lastname = user.getLastName();
		final String username = user.getUsername();
		
		//print header
		userStatisticsFileContents.append(String.format(Locale.ENGLISH, "\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\"", HeaderConstants.USERID, HeaderConstants.ACCOUNTID, HeaderConstants.REFNUMBER, HeaderConstants.FIRSTNAME, HeaderConstants.LASTNAME, HeaderConstants.USERNAME, HeaderConstants.CHANNEL, HeaderConstants.PROGRAMMERECORD, HeaderConstants.PROGRAMMERECORD_START, HeaderConstants.PROGRAMMERECORD_END, HeaderConstants.TIME_MINUTES));
		
		Map<ProgrammeRecord, Long> userStatisticsInPeriodMap = statisticsForUsers.get(user);
		
		//print statistics for programme records
		for(ProgrammeRecord programmeRecord : userStatisticsInPeriodMap.keySet()){
			Long seconds = userStatisticsInPeriodMap.get(programmeRecord);
			
			userStatisticsFileContents.append("\n" + String.format(Locale.ENGLISH, "\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",\"%s\",%s,%s,%f", userid, accountid, refnumber, firstname, lastname, username, programmeRecord.getProgramme().getChannel().getName(), programmeRecord.getProgramme().getName(), super.generateStatTimestamp(programmeRecord.getProgrammeStart(), true), super.generateStatTimestamp(programmeRecord.getProgrammeEnd(), true), seconds / 60.0));
		}
		
		return userStatisticsFileContents.toString();
	}

	protected static Date parseEPGTimestamp(String timestamp) throws IllegalArgumentException
	{
		try{
			return new SimpleDateFormat("dd-MM-yyyy HH:mm", Locale.ENGLISH).parse(timestamp);
		}catch(ParseException e){
			throw new IllegalArgumentException("invalid timestamp");
		}
	}
}