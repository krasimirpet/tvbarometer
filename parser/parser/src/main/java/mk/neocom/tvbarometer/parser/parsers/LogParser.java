package mk.neocom.tvbarometer.parser.parsers;

import java.io.File;
import java.io.IOException;

public interface LogParser
{	
	boolean bindEPGDocument(File file) throws IOException;
	
	boolean parseFile(File file) throws IOException;
	
	void writeStatistics(File directory) throws IOException;
}